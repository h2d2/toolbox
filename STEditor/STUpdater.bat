set SRC_DIR=%1
if ~%SRC_DIR%~==~~ set SRC_DIR=%INRS_DEV%\H2D2

set BIN_DIR=%~dp0
set BIN_DIR=%BIN_DIR:~0,-1%
python %BIN_DIR%\STUpdater.py %SRC_DIR%

if ~%MUC_AUTOMATIC_COMPILATION%~==~~ pause
