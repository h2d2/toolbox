#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
        xtrDir = os.path.join(home, 'toolbox/STEditor')
        if os.path.isdir(xtrDir):
            sys.path.append(xtrDir)
        else:
            raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import StringTbl

def xeqRecursion(dir, mdl = None):
    if (os.path.basename(dir) == 'H2D2' and
        os.path.isdir( os.path.join(dir, 'build') ) and
        os.path.isdir( os.path.join(dir, 'prjVisual') ) and
        os.path.isdir( os.path.join(dir, 'source') )):
        mdl = os.path.basename(dir)
        mdl = os.path.splitext(mdl)[0]

    fullPath = os.path.normpath(dir)
    if (os.path.isdir( os.path.join(fullPath, 'source') )):
        xeqAction(fullPath, mdl)
    else:
        for f in os.listdir(dir):
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                xeqRecursion(fullPath, mdl)

def xeqAction(dir, mdl):
    print('Scanning %s' % dir)
    data = StringTbl.StringTbl()
    modf = data.scanPath(dir, skipDir=['prjVisual'], type=StringTbl.Type.Message)
    if modf:
        print('   MSG_ modified')
        data.saveTbl()
    if not data.checkFull():
        print('   MSG_: Entries missing')

    data = StringTbl.StringTbl()
    modf = data.scanPath(dir, type=StringTbl.Type.Error)
    if modf:
        print('   ERR_ modified')
        data.saveTbl()
    if not data.checkFull():
        print('   ERR_: Entries missing')

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if argv == None: argv = sys.argv[1:]
    if len(argv) == 0: argv.append('.')
    for a in argv:
        xeqRecursion(a)

main()
