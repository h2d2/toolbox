#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

import PyFtn

LOGGER = logging.getLogger("INRS.IEHSS.Fortran.xtrapi")

class FileFor(PyFtn.File):

    def __init__(self, nomFic):
        PyFtn.File.__init__(self, nomFic)

    def parse(self, modules):
        self.doStripCmt = True
        self.doInclude  = False
        try:
            hdr = None
            while self:
                s1 = next(self)
                fnc = None
                fmo = None
                if s1.isModule():
                    fmo = H2D2FtnModule(self, hdr)
                elif s1.isSubModule():
                    fmo = H2D2FtnSubModule(self, hdr)
                elif s1.isFunction():
                    fnc = H2D2Function(self, hdr)
                elif s1.isSubroutine():
                    fnc = H2D2Subroutine(self, hdr)
                elif s1.isHeader():
                    hdr = PyFtn.Header(self)
                    if hdr.empty: hdr = None

                if fmo:
                    mdlName = fmo.getH2D2Module()
                    clsName = fmo.getH2D2Class()
                    if not mdlName: mdlName = '---'
                    if not clsName: clsName = '---'
                    mdl = modules[mdlName]
                    cls = mdl[clsName]
                    cls.setFileName(self.getFileName())
                    cls.addFtnModule(fmo)
                elif fnc:
                    mdlName = fnc.getH2D2Module()
                    clsName = fnc.getH2D2Class()
                    if not mdlName: mdlName = '---'
                    if not clsName: clsName = '---'
                    mdl = modules[mdlName]
                    cls = mdl[clsName]
                    cls.setFileName(self.getFileName())
                    cls.addMethod(fnc)
                    if fnc.getName()[-6:] == '_CTRLH':
                        cls.pure = True
                    else:
                        hdr = None

        except StopIteration:
            pass

class FileFc(PyFtn.File):
    declarations = {}   # Dico de PyFtn.Variables

    def __init__(self, nomFic):
        PyFtn.File.__init__(self, nomFic)

    def parse(self, modules):
        self.doStripCmt = True
        self.doInclude  = False
        try:
            hdr  = None
            desc = ''
            while self:
                s1 = next(self)
                cmn = None
                if s1.isCommon():
                    cmn = PyFtn.Common(self, hdr, FileFc.declarations)
                elif s1.isDeclaration():
                    dcl = PyFtn.Declaration(self, desc)
                    for v in dcl.getVariables():
                        FileFc.declarations[v.getName()] = v
                elif s1.isTagComment():
                    cmt = PyFtn.Comment(s1)
                    desc = cmt.getComment()
                elif s1.isHeader():
                    hdr = PyFtn.Header(self)
                    if hdr.empty: hdr = None

                if cmn:
                    if cmn.getName()[-4:] == "_CDT":
                        mdl = modules[cmn.getModule()]
                        cls = mdl[cmn.getClass()]
                        for a in cmn.getVars():
                            cls.addAttribut(a)
                    else:
                        hdr = None

        except StopIteration:
            pass

class H2D2Function(PyFtn.Function):
    def __init__(self, ftnSta, hdr=None, doParseCode=True):
        self.constructors = []
        self.allocations = []
        self.includes = []
        self.assign = {}
        super(H2D2Function, self).__init__(ftnSta, hdr, doParseCode)

    def __parseIf(self, sta):
        s = sta.getAsTxt().strip()
        if_ = PyFtn.FtnIf(s, sta)
        for b in if_:
            for ss in b[2]:
                self.parseStatement(ss)

    def __filterAsg(self, sta):
        s = sta.getAsTxt().strip()
        l = s.find("=")
        if l > 0:
            if s[:10] == "PARAMETER ": return
            if s[:10] == "PARAMETER(": return
            var = s[:l].strip()
            if var.find("(") >= 0: var = var[:var.find("(")].strip()
            val = s[l+1:].strip()
            if val.find("(") >= 0: val = val[:val.find("(")].strip()
            if PyFtn.String(var).isVar():
                self.assign[var] = val

    def parseStatement(self, sta):
        s = sta.getAsTxt().strip().replace(" ", "")
        if s[0:3] == "IF(":
            self.__parseIf(sta)
        elif s.find('_DTR(') > 0:
            dtr = s
            dtr = dtr[:dtr.find('_DTR(')+4]
            dtr = dtr[dtr.rfind('=')+1:]
            obj = s
            obj = obj[obj.find('_DTR(')+5:]
            obj = obj[:obj.rfind(')')]
            att = obj
            while att in self.assign:
                att = self.assign[att]
            self.constructors.append((dtr, obj, att))
        elif s.find('=SO_ALLC_ALL') > 0:
            alc = s
            alc = alc[alc.find('=SO_ALLC_ALL')+1:]
            alc = alc[:alc.find('(')]
            ptr = s
            ptr = ptr[ptr.find('=SO_ALLC_ALL')+1:]
            ptr = ptr[ptr.find('(')+1:]
            ptr = ptr[ptr.find(',')+1:]
            ptr = ptr[:ptr.rfind(')')]
            att = ptr
            while att in self.assign:
                att = self.assign[att]
            self.allocations.append((alc, ptr, att))
        elif s[0:8].upper() == "INCLUDE'":
            self.includes.append(s[8:-1])
        else:
            self.__filterAsg(sta)
        PyFtn.Function.parseStatement(self, sta)

class H2D2Subroutine(H2D2Function, PyFtn.Subroutine):
    def __init__(self, ftnSta, hdr=None, doParseCode=True):
        super(H2D2Subroutine, self).__init__(ftnSta, hdr, doParseCode)

class H2D2FtnModule(PyFtn.Module):
    PyFtn.Module.SUB_ITEMS['Function']  = H2D2Function
    PyFtn.Module.SUB_ITEMS['Subroutine']= H2D2Subroutine

    def __init__(self, mdlSta, hdr=None, doParseCode=True):
        super(H2D2FtnModule, self).__init__(mdlSta, hdr, doParseCode)

class H2D2FtnSubModule(PyFtn.SubModule, H2D2FtnModule):
    def __init__(self, mdlSta, hdr=None, doParseCode=True):
        super(H2D2FtnSubModule, self).__init__(mdlSta, hdr, doParseCode)

class H2D2Class:
    def __init__(self, n):
        self.file = ""
        self.name = n
        self.hdr = None
        self.attributs = []
        self.ftnModules= []
        self.methodes  = []
        self.pure = False
        self.parents = []
        self.enfants = []

    def addFtnModule(self, m):
        self.ftnModules.append(m)

    def addMethod(self, f):
        self.methodes.append(f)

    def addAttribut(self, f):
        self.attributs.append(f)

    def getName(self):
        return self.name

    def getHdr(self):
        return self.hdr

    def getFileName(self):
        return self.file

    def setFileName(self, f):
        self.file = f

    def __str__(self):
        return 'H2D2Class: ' + str(self.name)

    def __repr__(self):
        return 'H2D2Class: ' + str(self.name) + str(self.methodes)
        
class H2D2Module:
    def __init__(self, n):
        self.name = n
        self.classes = {}
        self.functions = []

    def __len__(self): return len(self.classes)
    def __getitem__(self, c): return self.classes.setdefault(c, H2D2Class(c))
    def __iter__(self): return self.classes.__iter__()
    def items(self): return self.classes.items()
    def values(self): return self.classes.values()
    def __next__(self): return next(self.classes)

    def __str__(self):
        return 'H2D2Module: ' + str(self.name) + str(self.classes)

    def getName(self):
        return self.name

    def connectHierarchy(self):
        parent = None
        for c in self.values():
            if c.pure: parent = c

        if parent is not None:
            for c in self.values():
                if not c.pure:
                    c.parents.append(parent)
                    parent.enfants.append(c)

class H2D2Modules:
    def __init__(self):
        self.modules = {}

    def __len__(self): return len(self.modules)
    def __getitem__(self, n): return self.modules.setdefault(n, H2D2Module(n))
    def __iter__(self): return self.modules.__iter__()
    def items(self): return self.modules.items()
    def values(self): return self.modules.values()
    def __next__(self): return next(self.modules)

    def __str__(self):
        return 'H2D2Modules: ' + str(self.modules)

    def connectHierarchy(self):
        for m in self.values():
            m.connectHierarchy()
