#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import re
import string
import sys

class Writer:
    TYPES = {
            'LOGICAL' : 'bool',
            'INTEGER' : 'int',
            'REAL*8'  : 'double',
            'CHARACTER*256' : 'string',
            }

    @staticmethod            
    def __trdType(t):
        return Writer.TYPES[t]

    #__trdType = PyFtn.Callable(__trdType)

    def __init__(self, fname):
        self.indent = 0
        self.indents = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
        if (fname):
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __writeLine(self, l):
        if (self.indent > 0):
            self.fout.write( '%s%s\n' % (self.indents[:self.indent], l) )
        else:
            self.fout.write( '%s\m' % (l) )

    def __writeAttr(self, attr):
        if (attr.visibility == PyFtn.Visibility.Private and not self.xtr_private): return 0
        if (attr.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return 0
        self.__writeLine('%s %s;' % (Writer.__trdType(attr.getType()), attr.getName()))
        return 1

    def __writeMethod(self, meth):
        if (meth.visibility == PyFtn.Visibility.Private and not self.xtr_private): return 0
        if (meth.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return 0

        self.__writeLine('%s %s();' % (Writer.__trdType(meth.getType()), meth.getName()))
##        if (meth.hdr is not None and meth.hdr.description):
##            plain, html = self.__writeHtmlTxt(meth.getDecl(), meth.hdr.description)
##            print '\t\t\t\t\t\t<HTMLProperty displayName="Documentation" name="documentation" plainTextValue="%s" value="%s"/>' % (plain, html)
        return 1

    def __writeClass(self, cls):
        h = ', '.join(cls.parents).strip()
        if (len(h) > 0):
            h = ' : '.join( [cls.getName(), h ] )
        else:
            h = cls.getName()
        self.__writeLine('class %s' % h)
        self.__writeLine('{')
#        if cls.pure:
#            self.__writeLine('<BooleanProperty displayName="Abstract" name="abstract" value="true"/>')
##        if (cls.getHdr()):
##            print '\t\t\t\t<HTMLProperty displayName="Documentation" name="documentation" plainTextValue="%s"/>' % 'TODO!!!'

        v = PyFtn.Visibility.Public
        self.__writeLine('%s:' % v)
        n = 0
        self.indent += 1
        for a in cls.attributs:
            if (a.getVisibility() == v):
                n += self.__writeAttr(a)
        if (n > 0): self.__writeLine('')
        n = 0
        for m in cls.methodes:
            if (m.getVisibility() == v):
                n += self.__writeMethod(m)
        self.indent -= 1

        self.__writeLine('')
        v = PyFtn.Visibility.Private
        self.__writeLine('%s:' % v)
        n = 0
        self.indent += 1
        for a in cls.attributs:
            if (a.getVisibility() == v):
                n += self.__writeAttr(a)
        if (n > 0): self.__writeLine('')
        n = 0
        for m in cls.methodes:
            if (m.getVisibility() == v):
                n += self.__writeMethod(m)
        self.indent -= 1

        self.__writeLine('};')

    def __writeModule(self, mdl):
        self.__writeLine('//===============================')
        self.__writeLine('// Module: %s' % (mdl.getName()))
        self.__writeLine('//===============================')
        self.__writeLine('')
        for c in mdl.values():
            self.__writeClass(c)

    def write(self, mdls, xtr_public = True, xtr_private = False):
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        for mdl in mdls.values():
            self.__writeModule(mdl)
