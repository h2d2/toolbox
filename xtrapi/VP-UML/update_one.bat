@echo off
if [%3]==[] goto usage
if [%2]==[] goto usage
if [%1]==[] goto usage
set TGTMDL=%1
set TGTFIL=%2
set SRCFIL=%3
set INPPRM=%4

set XTRDIR=%INRS_DEV%\tools\xtrapi
set XMLDIR=%XTRDIR%\VP-UML

set XTRAPI=%XTRDIR%\xtrapi.py
set UPDXML=%XMLDIR%\updateXML.py
set TMPFIL=%XMLDIR%\~inrs-tmp.xml

set XTRPRM=
set XTRPRM=%XTRPRM% --extract-public
set XTRPRM=%XTRPRM% --extract-private
set XTRPRM=%XTRPRM% --format xml
set XTRPRM=%XTRPRM% --kword modul=%TGTMDL%
set XTRPRM=%XTRPRM% %INPPRM%

python %XTRAPI% @%INRS_DEV%\H2D2\h2d2.i %XTRPRM% -o %TMPFIL% %SRCFIL%
python %UPDXML% %TMPFIL% %TGTFIL%
goto end

:usage
echo usage:
echo     update_one target_module target_file source_files [supplementary_parameters]
echo example:
echo     update_one "dt" "dt.xml" h2d2_krnl\source\dt*.f*
goto end

:end