#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import string
import sys
import os

def if_else(p, n1, n2):
    if (p): return n1
    return n2

class Writer:
    def __init__(self, fname):
        self.fname = fname
        self.fout = None

    def __openFile(self):
        if (self.fout): return
        if (self.fname):
            self.fout = open(self.fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __writeLine(self, l):
        self.fout.write("%s\n" % (l))

    def __writeHdr(self, cls, includes):
        cname = cls.getName()
        mname = "%s_RAZ" % (cname)
        fname = os.path.basename(cls.getFileName())
        fname = os.path.splitext(fname)[0]
        self.__writeLine("""\
C************************************************************************
C Sommaire: Remise à zéro des attributs (eRaze)
C
C Description:
C     La méthode privée %s (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************""" % mname)
        self.__writeLine("      FUNCTION %s(HOBJ)" % mname)
        self.__writeLine("")
        self.__writeLine("      IMPLICIT NONE")
        self.__writeLine("")
        self.__writeLine("      INTEGER HOBJ")
        self.__writeLine("")
        if (len(includes) > 3):
            for i in includes:
                self.__writeLine("      INCLUDE '%s'" % i)
        else:
            self.__writeLine("      INCLUDE '%s.fi'" % fname)
            self.__writeLine("      INCLUDE 'err.fi'")
            self.__writeLine("      INCLUDE '%s.fc'" % fname)
        self.__writeLine("")
        self.__writeLine("      INTEGER IOB")
        self.__writeLine("C------------------------------------------------------------------------")
        self.__writeLine("D     CALL ERR_PRE(%s_HVALIDE(HOBJ))" % cname)
        self.__writeLine("C------------------------------------------------------------------------")
        self.__writeLine("")

    def __writeFtr(self, cls):
        cname = cls.getName()
        mname = "%s_RAZ" % (cname)
        self.__writeLine("      %s = ERR_TYP()" % mname)
        self.__writeLine("      RETURN")
        self.__writeLine("      END")
        self.__writeLine("")

    def __writeOneAttrib(self, cls, attr, val = None):
        var = attr.getName()
        if   (attr.getType()[0:7] == "INTEGER"):
            if not val: val = "0"
        elif (attr.getType() == "LOGICAL"):
            if not val: val = ".FALSE."
        elif (attr.getType() == "REAL*8"):
            if not val: val = "0.0D0"
        elif (attr.getType()[0:10] == "CHARACTER*"):
            if not val: val = "' '"
        else:
            raise InvalideType("%s %s" % (attr.getType(), attr.getName()))
        self.__writeLine("      %s(IOB) = %s" % (var, val))

    def __writeAttribs(self, cls, assign):
        cname = cls.getName()
        self.__writeLine("      IOB = HOBJ - %s_HBASE" % cname)
        self.__writeLine("")
        for a in cls.attributs:
            v = assign.get(a.getName(), None)
            self.__writeOneAttrib(cls, a, v)
        self.__writeLine("")

    def __copyUptoFunction(self, fnc):
        self.fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineFirst()-1):
            l = next(self.fic)
            self.__writeLine(l[:-1])
        self.fic.close()

    def __copyIncludingFunction(self, fnc):
        self.fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineLast()-1):
            l = next(self.fic)
            self.__writeLine(l[:-1])
        self.fic.close()

    def __copyFromFunction(self, fnc):
        self.fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineLast()-1):
            l = next(self.fic)
        for l in self.fic:
            self.__writeLine(l[:-1])
        self.fic.close()

    def __writePKLClass(self, cls, fRaz, fDtr, assign = {}, includes  = [] ):
        self.__openFile()
        if (fRaz):
            self.__copyUptoFunction(fRaz)
        else:
            self.__copyIncludingFunction(fDtr)
        self.__writeHdr(cls, includes)
        self.__writeAttribs(cls, assign)
        self.__writeFtr(cls)
        if (fRaz):
            self.__copyFromFunction(fRaz)
        else:
            self.__copyFromFunction(fDtr)

    def __writeClass(self, cls):
        def filterMethod(meths, k):
            l = [f for f in meths if f.getKind() == k]
            return if_else(len(l) == 0, None, l[0])
        fRaz = filterMethod(cls.methodes, PyFtn.MethodKind.RAZ)
        fDtr = filterMethod(cls.methodes, PyFtn.MethodKind.DTR)
        fRst = filterMethod(cls.methodes, PyFtn.MethodKind.RST)
        if fDtr and (fRaz or fRst):
            f = if_else(fRaz, fRaz, fRst)
            inc = []
            if (fRaz): inc = fRaz.includes
            self.__writePKLClass(cls, fRaz, fDtr, f.assign, inc)

    def __writeModule(self, mdl):
        for c in mdl.values():
            self.__writeClass(c)

    def write(self, mdls, xtr_public = True, xtr_private = False, **kwargs):
        for mdl in mdls.values():
            self.__writeModule(mdl)
