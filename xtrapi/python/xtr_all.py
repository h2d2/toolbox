#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
         xtrDir = os.path.join(home, 'toolbox/xtrapi')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrapi

def xeqRecursion(dir, mdl = None):
    if (os.path.isdir( os.path.join(dir, 'build') )):
        mdl = os.path.basename(dir)
        mdl = os.path.splitext(mdl)[0]

    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            xeqRecursion(fullPath, mdl)
        elif (os.path.splitext(fullPath)[1].lower() == '.for'):
            xeqAction(fullPath, mdl)

def xeqAction(dir, mdl):
    inp = dir
    out = os.path.basename(inp)
    out = os.path.splitext(out)[0]
    out = '.'.join( [out, 'py_'] )

    inc = os.path.join(home, 'H2D2/h2d2.i')
    args = '@%s -f python -o %s %s' % (inc, out, inp)
    args = args.split(' ')
    xtrapi.main(args)

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if (argv == None): argv = sys.argv[1:]
    if (len(argv) == 0): argv.append('.')
    for a in argv:
        xeqRecursion(a)

main()
