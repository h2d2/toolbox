#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait la structure de classes des modules de H2D2
"""

import os
import sys

import logging
LOGGER = logging.getLogger("INRS.IEHSS.Fortran")

try:
    devDir = os.environ['INRS_DEV']
    if os.path.isdir(devDir):
        xtrDir = os.path.join(devDir, 'toolbox')
        if os.path.isdir(xtrDir):
            sys.path.append(xtrDir)
        else:
            raise RuntimeError('Not a valid directory: %s' % xtrDir)

        xtrDir = os.path.join(devDir, 'toolbox/xtrapi')
        if os.path.isdir(xtrDir):
            sys.path.append(xtrDir)
        else:
            raise RuntimeError('Not a valid directory: %s' % xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s' % os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import H2D2Module
import WriterCmc
import WriterCPP
import WriterDecl
import WriterHtml
import WriterTxt
import WriterPython
import WriterXML
import WriterPKL
import WriterRAZ

import glob
import optparse
rpdb2_imported = False
try:
    import rpdb2
    rpdb2_imported = True
except ImportError:
    pass

__package__ = 'xtrapi'
__version__ = '19.02'

def xeq(modules, args, recurse=False):
    for arg in args:
        arg = arg.strip()
        if len(arg) == 0: continue

        arg = os.path.normpath(arg)
        for f in glob.glob(arg):
            typ = os.path.splitext(f)[1]
            if (typ.lower() in ['.for', '.f90']):
                LOGGER.info('Parsing: %s', f)
                s = H2D2Module.FileFor(f)
                s.parse(modules)
            elif typ == '.fc':
                LOGGER.info('Parsing: %s', f)
                s = H2D2Module.FileFc(f)
                s.parse(modules)
            elif recurse and os.path.isdir(f):
                LOGGER.debug('Recurse: %s', f)
                xeq(modules, [ os.path.join(f, '*') ], recurse)

def main(opt_args=None):
    print('%s %s' % (__package__, __version__))

    def opt_kw(option, opt_str, value, parser):
        k,v = value.split('=')
        parser.values.kwd[k] = v

    # ---  Parse les options
    usage  = '%s [options]' % __package__
    parser = optparse.OptionParser(usage)
    parser.add_option("-I", "--include", dest="inc", default=[], action='append',
                  help="include PATH", metavar="INC")
    parser.add_option("-f", "--format", dest="fmt", default="html",
                  help="output FORMAT", metavar="FORMAT")
    parser.add_option("-d", "--debug", dest="dbg", default=False, action='store_true',
                  help="debug the run", metavar="DEBUG")
    parser.add_option("-o", "--output", dest="out", default=None,
                  help="output FILE", metavar="FILE")
    parser.add_option("-p", "--extract-public", dest="pbl", default=True, action='store_true',
                  help="extract public variables and functions")
    parser.add_option("--no-extract-public", dest="pbl", action='store_false',
                  help="don't extract public variables and functions")
    parser.add_option("-q", "--extract-private", dest="prv", default=False, action='store_true',
                  help="extract private variables and functions")
    parser.add_option("--no-extract-private", dest="prv", action='store_false',
                  help="don't extract private variables and functions")
    parser.add_option("-k", "--kword", dest="kwd", type='string', default={}, action="callback", callback=opt_kw,
                  help="keyword argments passed to the writer")
    parser.add_option("-r", "--recurse", dest="rec", default=False, action='store_true',
                  help="recurse through directories")

    # ---  Inclus les fichiers réponse
    if not opt_args: opt_args = sys.argv[1:]
    done = False
    while not done:
        rmv = []
        rsp = []
        for a in opt_args:
            if (len(a) > 0 and a[0] == '@'):
                f = open(a[1:])
                l = f.readlines()
                f.close()
                for i in l:
                    rsp.extend( [ t.strip() for t in i.split() ] )
                rmv.append(a)
        for a in rmv: opt_args.remove(a)
        opt_args.extend(rsp)
        if len(rsp) == 0: done = True

    # --- Parse les arguments de la ligne de commande
    (options, args) = parser.parse_args(opt_args)

    H2D2Module.FileFor.addIncPath(options.inc)

    if options.dbg:
        if not rpdb2_imported:
            raise RuntimeError('rpdb2 must be installed to debug')
        else:
            rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted=True)

    modules = H2D2Module.H2D2Modules()
    xeq(modules, args, options.rec)
#    apis.sort(lambda x,y: cmp(x.name, y.name))
    modules.connectHierarchy()

    w = None
    if options.fmt == 'cmc':    w = WriterCmc.Writer (options.out)
    if options.fmt == 'cpp':    w = WriterCPP.Writer (options.out)
    if options.fmt == 'c++':    w = WriterCPP.Writer (options.out)
    if options.fmt == 'decl':   w = WriterDecl.Writer(options.out)
    if options.fmt == 'txt':    w = WriterTxt.Writer (options.out)
    if options.fmt == 'html':   w = WriterHtml.Writer(options.out)
    if options.fmt == 'python': w = WriterPython.Writer(options.out)
    if options.fmt == 'xml':    w = WriterXML.Writer (options.out)
    if options.fmt == 'pkl':    w = WriterPKL.Writer (options.out)
    if options.fmt == 'raz':    w = WriterRAZ.Writer (options.out)
    if w:
        w.write(modules, xtr_public=options.pbl, xtr_private=options.prv, **options.kwd)

if __name__ == '__main__':
    args = ' '.join( ('@' + os.path.normpath(os.path.join(devDir, 'H2D2/h2d2.i')),
                      '-f decl',
                      '-o _new.cpp',
                      'sv2d_cl123.*') )
                      #os.path.normpath(os.path.join(devDir, 'H2D2/h2d2_elem/source/lmgeom/lmgeom.*'))) )
    args = args.split(' ')

    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    main(args)
    #main()
