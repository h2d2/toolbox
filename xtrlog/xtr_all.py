#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import re
import sys

logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
         xtrDir = os.path.join(home, 'toolbox')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import PyFtn

def xeqRecursion(dir, zones):
    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            xeqRecursion(fullPath, zones)
        elif (os.path.splitext(fullPath)[1].lower() == '.for'):
            xeqAction(fullPath, zones)

def xeqAction(dir, zones):
    print('Scanning %s' % dir)

    fic = PyFtn.File(dir)
    fic.setInclude(False)
    fic.setStripCmt(True)
    try:
        ldr = r"\s+LOG_ZNE\s*=\s*'"
        wrd = r"[a-z][a-z0-9]*"
        wds = r"%s(\.%s)*" % (wrd, wrd)
        trl = r"'"
        ptrn = re.compile( r"%s%s%s" % (ldr, wds, trl) )
        while (fic):
            st = str(next(fic))
            tk = ptrn.search(st)
            if (tk):
               tk = tk.group()
               tk = tk.split('=')[1].strip()[1:-1]
               zones.add(tk)
    except StopIteration:
        pass

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if argv == None: argv = sys.argv[1:]
    if len(argv) == 0: argv.append('.')
    zones = set()
    for a in argv:
        xeqRecursion(a, zones)

    # --- Complète avec les intermédiaires manquants
    tmp = [ z for z in zones]
    tmp = sorted(tmp)
    zones = []
    zones.append('h2d2')
    for entry1 in tmp:
        entry0 = zones[-1]
        ntok = len(entry1.split('.')) - 1
        for it in range(ntok):
            tok = '.'.join( entry1.split('.', it+1)[:it+1] )
            if tok not in entry0:
                zones.append(tok)
        if entry1 != zones[-1]: zones.append(entry1)
    #for z in zones:
    #    print(z)

    # ---  l'arborescence
    print('--> Write results to file: "log_zones.txt"')
    fout = open('log_zones.txt', 'wt', encoding='utf-8')
    indent = 0
    fout.write("C %s<ul>\n" % (' '*indent,))
    indent += 3
    fout.write("C %s<li>%s</li>\n" % (' '*indent, zones[0]))
    for prev, zone in zip(zones, zones[1:]):
        nprev = len(prev.split('.'))
        nzone = len(zone.split('.'))
        if nzone > nprev:
            fout.write("C %s<ul>\n" % (' '*indent,))
            indent += 3
        elif nzone < nprev:
            while nzone < nprev:
                indent -= 3
                fout.write("C %s</ul>\n" % (' '*indent,))
                nprev -= 1
        fout.write("C %s<li>%s</li>\n" % (' '*indent, zone))

    while indent > 0:
        indent -= 3
        fout.write("C %s</ul>\n" % (' '*indent,))

main()
