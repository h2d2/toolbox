#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Fortran Parser
The classes represents different Fortran structure needed
to extract information from the code
"""

__version__ = "21.xxdev"

import enum
import os.path
import re
import sys

import logging
LOGGER = logging.getLogger("INRS.IEHSS.Fortran.Parser")

Visibility = enum.Enum('Visibility', ('Public', 'Private'))
MethodKind = enum.Enum('MethodKind', ('General', '000', '999', 'PKL', 'UPK', 'CTR', 'DTR', 'RAZ', 'INI', 'RST'))
VarChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_'

def sansAccents(i):
    try:
        r = str(i)
    except UnicodeEncodeError:
        r = i.encode(sys.getdefaultencoding(), 'replace')
    return r

## class Callable:
##     """
##     Callable class for static method work-around
##     """
##     def __init__(self, anycallable):
##         self.__call__ = anycallable

class FileNotFound(IOError):
    def __init__(self, *args):
        IOError.__init__(self)
        self.args = args

class FortranParseError(Exception):
    def __init__(self, *args):
        Exception.__init__(self)
        self.args = args

class String:
    def __init__(self, s):
        self.s = s

    def __str__(self):
        return self.s

    def getBlock(self, ltok, rtok):
        inCmt = False
        nPar = 0
        i = sys.maxsize
        for i in range(0, len(self.s)):
            c = self.s[i]
            if c == '\'': inCmt = not inCmt
            if not inCmt:
                if c == ltok: nPar +=1
            if nPar == 1:
                break
        for j in range(i+1, len(self.s)):
            c = self.s[j]
            if c == '\'': inCmt = not inCmt
            if not inCmt:
                if c == ltok: nPar +=1
                if c == rtok: nPar -=1
            if nPar == 0:
                ret = self.s[i+1:j]
                break
        return ret

    def isVar(self):
        for c in self.s.strip():
            if c not in VarChar: return False
        return True

class Statement:
    """
    A Fortran statement
    """
    def __init__(self, s):
        self.sta = s           # statement

    def clearTxt(self):
        self.sta = ''

    def getAsTxt(self):
        return self.sta

    def isModule(self):
        if self.isComment(): return False
        s = self.sta.strip()
        s = s.replace('  ', ' ')
        return len(s) > 8 and s[0:7] in ('MODULE ',)

    def isSubModule(self):
        if self.isComment(): return False
        s = self.sta.strip()
        s = s.replace('  ', ' ')
        return len(s) > 10 and s[0:10] in ('SUBMODULE ', 'SUBMODULE(')

    def isType(self):
        if self.isComment(): return False
        s = self.sta.strip()
        s = s.replace('  ', ' ')
        if len(s) < 4 or  s[0:4] not in ('TYPE',): return False
        if len(s) > 5 and s[0:5] in ('TYPE(',): return False
        if len(s) > 6 and s[0:6] in ('TYPE (',): return False
        return len(s) > 5 and s[0:5] in ('TYPE ', 'TYPE:', 'TYPE,')

    def isInterface(self):
        if self.isComment(): return False
        s = self.sta.strip().split()
        if not s: return False
        if len(s) > 0 and s[0] == 'INTERFACE': return True
        if len(s) > 1 and s[0] != 'END' and 'INTERFACE' in s: return True
        return False

    def isBlockData(self):
        if self.isComment(): return False
        s = self.sta.strip()
        s = s.replace('  ', ' ')
        return len(s) > 11 and s[0:11] == 'BLOCK DATA '

    def isComment(self):
        s = self.sta
        if s[0] in ['c', 'C', 'd', 'D']: return True
        return s.strip()[0] in ['!']

    def isFunction(self):
        if self.isComment(): return False
        s = self.sta.strip().split()
        if not s: return False
        if len(s) > 0 and s[0] == 'FUNCTION': return True
        if len(s) > 1 and s[0] != 'END' and 'FUNCTION' in s: return True
        return False

    def isSubroutine(self):
        if self.isComment(): return False
        s = self.sta.strip().split()
        if len(s) > 0 and s[0] == 'SUBROUTINE': return True
        if len(s) > 1 and s[0] != 'END' and 'SUBROUTINE' in s: return True
        return False

    def isTagComment(self):
        s = self.sta.strip()
        return len(s) > 9 and s[0:9] == '<comment>'

    def isTagInclude(self):
        s = self.sta.strip()
        return len(s) > 9 and s[0:9] == '<include>'

    def isTagPython(self):
        s = self.sta.strip()
        return len(s) > 8 and s[0:8] == '<python>'

    def isInclude(self):
        s = self.sta.strip()
        return len(s) > 8 and s[0:8] == 'INCLUDE '

    def isHeader(self):
        s = self.sta.strip()
        return s[0:8] == '<header>'

    def isCommon(self):
        s = self.sta.strip()
        return len(s) > 7 and s[0:7] == 'COMMON '

    def isDeclaration(self):
        s = self.sta.strip()
        s = s.upper()
        return s[0: 8] in ('INTEGER ', 'INTEGER*', 'INTEGER(') or \
               s[0: 8] in ('LOGICAL ', 'LOGICAL*', 'LOGICAL(') or \
               s[0: 5] in ('REAL ', 'REAL*', 'REAL(') or \
               s[0:10] in ('CHARACTER', 'CHARACTER*', 'CHARACTER(') or \
               s[0: 5] in ('TYPE ',  'TYPE(') or \
               s[0: 6] in ('CLASS ', 'CLASS(')

    def isProcedure(self):
        s = self.sta.strip()
        s = s.upper()
        return s[0:10] in ('PROCEDURE ', 'PROCEDURE,', 'PROCEDURE(') or \
               s[0:17] in ('MODULE PROCEDURE ', 'MODULE PROCEDURE:')

    def isVisibility(self):
        s = self.sta.strip()
        s = s.upper()
        return s == 'PUBLIC'  or s[0:7] == 'PUBLIC '  or s[0:8] == 'PUBLIC::' or \
               s == 'PRIVATE' or s[0:8] == 'PRIVATE ' or s[0:9] == 'PRIVATE::'

    def __str__(self):
        return sansAccents( self.getAsTxt() )

class File(Statement):
    """
    A Fortran file to be parsed
    """

    includePath = [ '' ]
    excludeFile = [ '' ]
    foundPath   = {}
    cachedFiles = {}

    @staticmethod
    def addExcFile(lst):
        for f in lst:
            f = f.strip()
            if f[0] == '"' and f[-1] == '"': f = f[1:-1]
            if f[0] == "'" and f[-1] == "'": f = f[1:-1]
            File.excludeFile.append(f)

    @staticmethod
    def addIncPath(lst):
        devDir = os.environ['INRS_DEV']
        lxtDir = os.environ['INRS_LXT']
        for f in lst:
            f = f.strip()
            if devDir: f = f.replace('$(INRS_DEV)', devDir)
            if lxtDir: f = f.replace('$(INRS_LXT)', lxtDir)
            if f[0] == '"' and f[-1] == '"': f = f[1:-1]
            if f[0] == "'" and f[-1] == "'": f = f[1:-1]
            File.includePath.append(os.path.normpath(f))

    #addIncPath = Callable(addIncPath)
    #addExcFile = Callable(addExcFile)

    class OneFile:
        class CacheEntry:
            def __init__(self):
                self.nhit  = 0
                self.curl  = None
                self.lines = []
                self.llen  = 0

            def write(self, l):
                self.lines.append(l)

            def open(self):
                self.nhit += 1
                self.curl = -1

            def close(self):
                self.curl = None
                self.llen = len(self.lines)

            def __iter__(self):
                return self

            def __next__(self):
                self.curl += 1
                if self.curl == self.llen: raise StopIteration
                return self.lines[self.curl]

            next = __next__         # For Python 2

        def __init__(self, fileName, doStripCmt):
            self.fprc = None
            self.fic  = None     # Initialized here 'cause of exceptions

            if fileName in File.excludeFile:
                raise FileNotFound("Excluded file: %s" % fileName)

            try:
                fullPath = File.foundPath[fileName]
            except Exception:
                fullPath = None
                for p in File.includePath:
                    fp = os.path.join(p, fileName)
                    LOGGER.debug('File.__init__: searching file as %s', fp)
                    if os.path.isfile(fp):
                        fullPath = fp
                        break
                if not fullPath:
                    File.excludeFile.append(fileName)
                    raise FileNotFound("Invalid file: %s" % fileName)
                else:
                    File.foundPath[fileName] = fullPath

            self.doStripCmt = doStripCmt    # Remove comment, except for header, tags, etc..
            self.inCmtBlk = False           # True if in a comment block

            self.name = fileName
            self.shrt= os.path.basename(self.name)
            self.full= fullPath
            self.open()

            self.sttmLine = 0
            self.currLine = 0
            self.lastLine = 1000000
            self.sta = ''           # actual statement
            self.lah = ''           # look ahead

        def __del__(self):
            self.close()

        def __eq__(self, other):
            try:
                return self.full == other.full
            except Exception:
                return self.name == other

        def __ne__(self, other):
            return not self == other

        def close(self):
            if self.fic:  self.fic.close()
            if self.fprc: self.fprc.close()
            self.fic = None

        def open(self):
            if self.fprc:
                self.fprc.open()
            else:
                self.fic  = open(self.full, 'r', encoding='utf-8')
                if os.path.splitext(self.full)[1] in ['.fi', '.fc']:
                    self.fprc = File.OneFile.CacheEntry()

        def __iter__(self):
            return self

        def __next__(self):
            if self.fic:
                l = self.__nextInSrc()
                if self.fprc: self.fprc.write(l)
            else:
                l = self.__nextInPrc()
            return l

        next = __next__     # For Python 2

        def __nextInPrc(self):
            if self.currLine >= self.lastLine:
                raise StopIteration
            return next(self.fprc)

        def __nextInSrc(self):
            # --- Read first line
            l = self.__readLine()
            self.sttmLine = self.currLine

            # --- Read continuation lines
            done = False
            while not done:
                if l[-1] == '&':
                    if not self.inCmtBlk:
                        la = self.__readLine()
                        la = la.lstrip()
                        l = l[:-1].rstrip()
                        l = ' '.join([l, la])
                elif l[0:6] == '<crlf>':
                    if self.inCmtBlk:
                        self.inCmtBlk = False
                        l = ''
                        done = True
                    else:
                        l = ''
                elif l[0:8] == '<python>':
                    if l.find('</python>') >= 0:
                        self.inCmtBlk = False
                        done = True
                    else:
                        self.inCmtBlk = True
                        la = self.__readLine()
                        la = la.lstrip()
                        l = ' '.join([l, la])
                elif l[0:9] == '<comment>':
                    if l.find('</comment>') >= 0:
                        self.inCmtBlk = False
                        done = True
                    else:
                        self.inCmtBlk = True
                        la = self.__readLine()
                        #la = la.lstrip()
                        l = '<crlf>'.join([l, la])
                elif l[0:9] == '<include>':
                    if l.find('</include>') >= 0:
                        self.inCmtBlk = False
                        done = True
                    else:
                        self.inCmtBlk = True
                        la = self.__readLine()
                        la = la.lstrip()
                        l = ' '.join([l, la])
                elif l[0:9] == '*********':
                    self.inCmtBlk = not self.inCmtBlk
                    if self.inCmtBlk:
                        l = '<header>'
                    else:
                        l = '</header>'
                    done = True
                else:
                    try:    # Don't raise exception in look-ahead
                        la = self.__lahLine()
                        # ---  Ligne de continuation
                        if (len(la) > 6 and la[0:5] == '     ' and la[5] != ' '):
                            la = self.__readLine()
                            la = la[6:]
                            la = la.lstrip()
                            l = ' '.join([l, la])
                        # ---  Lignes spéciales
                        elif len(la) > 9 and la[0:9] == 'C$pragma ':
                            done = True
                        elif len(la) > 5 and la[0:5] == 'CDEC$':
                            done = True
                        # ---  Ligne de commentaire
                        elif not self.inCmtBlk:
                            la = self.__stripComment(la).rstrip()
                            if len(la) == 0:
                                pass
                            else:
                                done = True
                        else:
                            done = True
                    except Exception:
                        done = True

            # --- Keep result
            self.sta = l

            LOGGER.debug('[%s:%4d] %s', self.shrt, self.sttmLine, l[0:50])
            return self.sta

        def __readLine(self):
            """ Read one line """
            l = ''
            while len(l) == 0:
                l = self.__lahLine()
                self.lah = ''
                l = self.__stripComment(l)
                l = l.rstrip()
            return l

        def __lahLine(self):
            """ Look-ahead line """
            l = None
            if len(self.lah) > 0:
                l = self.lah
                self.lah = ''
            else:
                if self.currLine >= self.lastLine:
                    raise StopIteration
                try:
                    l = next(self.fic) #.decode('utf-8')
                except UnicodeDecodeError:
                    raise FortranParseError("Error decoding UTF-8: %s at line %d" % (self.name, self.currLine))
                self.currLine += 1
                if len(l) > 1: l = l[0:-1]
                l = l.rstrip()
                self.lah = l
            return l

        def __stripComment(self, l):
            """ Strip the comments from a line"""
            if len(l) == 0:
                if self.inCmtBlk:
                    return '<crlf>'
                return ''

            if self.inCmtBlk:
                if l[0] == '!': return l[1:] #.lstrip()
                if l[0] == 'd': return l[1:] #.lstrip()
                if l[0] == 'D': return l[1:] #.lstrip()
                if l[0] == 'c': return l[1:] #.lstrip()
                if l[0] == 'C': return l[1:] #.lstrip()

            if self.doStripCmt:
                if l[0] == 'd': return ''
                if l[0] == 'D': return ''

            if l[0] in ['c', 'C', '!'] or l.strip().startswith('!'):
                l1 = l.strip()
                l1 = l1[1:].strip()
                if l1[0:8] == '<python>': return l1
                if l1[0:9] == '<comment>': return l1
                if l1[0:9] == '<include>': return l1
                if l1[0:9] == '*********': return l1
                if self.doStripCmt: return ''
                return l

            # tenir compte des '
            try:
                i2 = l.rindex('!')
            except ValueError:
                return l
            try:
                i1 = l.index("'")
                if i2 < i1: return l[0:i2]
                inCmt = True
                for i in range(i1+1, len(l)):
                    if l[i] == "'": inCmt = not inCmt
                    if not inCmt and l[i] == '!': return l[0:i]
            except ValueError:
                i2 = l.index('!')       # no quotes; get first '!'
                return l[0:i2]
            return l

    def __init__(self, fileName):
        super(File, self).__init__('')
        self.__name = fileName      # Nom du fichier racine
        self.__doInclude  = True
        self.__doStripCmt = True
        self.__files = [ None, File.OneFile(self.__name, self.__doStripCmt) ]

    def isInCmtBlk(self):
        return self.__files[-1].inCmtBlk

    def __iter__(self):
        return self

    def __next__(self):
        try:
            self.sta = next(self.__files[-1])
        except StopIteration:
            LOGGER.debug('File.next: pop %s', self.__files[-1].name)
            f = self.__files.pop()
            f.close()
            File.cachedFiles[f.name] = f
            LOGGER.debug('File.next: stack size now %d', len(self.__files)-1)
            if not self.__files[-1]: raise StopIteration

        # --- Manage includes
        if self.doInclude:
            if self.isInclude():
                self.__openFtnInclude()
            elif self.isTagInclude():
                self.__openTagInclude()
        return self

    next = __next__     # For Python 2

    def __openFtnInclude(self):
        fname = self.sta.lstrip()
        fname = fname[7:].lstrip()
        fname = fname[1:-1]
        if fname in self.__files: raise FortranParseError('Circular include on: %s' % fname)
        LOGGER.debug("   Including: %s", fname)
        f = None
        try:
            f = File.cachedFiles[fname]
            f.open()
            LOGGER.debug("   Cache hit: %s", fname)
        except KeyError:
            try:
                f = File.OneFile(fname, self.__doStripCmt)
            except FileNotFound as e:
                LOGGER.info('%s - Skipping', e)
        if f:
            self.__files.append(f)

    def __openTagInclude(self):
        doInclude = self.doInclude
        self.doInclude = False

        t = TagString(self)
        s = t.getTxt()
        func, fname = s.split('@')
        if self.__files.count(fname) > 2: raise FortranParseError('Circular include on: %s' % fname)
        LOGGER.info("   Including: %s for %s", fname, func)

        # --- Open file and search for begin-end of function
        LOGGER.debug("      Scanning for %s", func)
        self.__files.append( File.OneFile(fname, self.__doStripCmt) )
        fc = self.__files[-1]
        ninc = len(self.__files)
        done = False
        while not done:
            s = next(self)
            if len(self.__files) != ninc: raise FortranParseError('Function %s not found in %s' % (func, fname))

            f = None
            if s.isHeader():
                h = Header(s)
            elif s.isModule():
                f = Module(s, doParseCode=False)
            elif s.isSubModule():
                f = SubModule(s, doParseCode=False)
            elif s.isFunction():
                f = Function(s, doParseCode=False)
            elif s.isSubroutine():
                f = Subroutine(s, doParseCode=False)

            if f:
                sttmLine = fc.sttmLine
                f.skipCode(s)
                lastLine = fc.currLine
                if f.getName() == func:
                    done = True
                    LOGGER.debug("      Found at %d to %d", sttmLine, lastLine)

        # --- Reposition the file at function begin
        if done:
            if self.__files[-1].name == fname:
                self.__files.pop()
            self.__files.append( File.OneFile(fname, self.__doStripCmt) )
            fc = self.__files[-1]
            while fc.sttmLine != sttmLine:
                s = next(self)
            assert fc.sttmLine == sttmLine
            fc.lastLine = lastLine

        self.doInclude = doInclude

    def getFileName(self):
        return self.__name

    def getCurrentFile(self):
        return self.__files[-1].full

    def getCurrentLine(self):
        return self.__files[-1].currLine

    def getStatementStartLine(self):
        return self.__files[-1].sttmLine

    def setStripCmt(self, b):
        self.__doStripCmt = b
        for f in self.__files[1:]: f.doStripCmt = b

    def getStripCmt(self):
        return self.__doStripCmt

    def setInclude(self, b):
        self.__doInclude = b

    def getInclude(self):
        return self.__doInclude

    doStripCmt = property(getStripCmt, setStripCmt)
    doInclude  = property(getInclude, setInclude)

    def __str__(self):
        """ __str__ redefined to resolve multiple inheritance ambiguity"""
        return Statement.__str__(self)

class TagString:
    def __init__(self, s):
        self.sta = s

    def getTag(self):
        l = self.sta.getAsTxt()
        return l[1:l.find('>')]

    def getTxt(self):
        l = self.sta.getAsTxt()
        l = l[l.find('>')+1:]
        l = l[:l.rfind('<')]
        return l.strip()

    def __str__(self):
        return sansAccents( self.getTxt() )

class Header:
    def __init__(self, ftnSta):
        if not ftnSta.isHeader():
            raise FortranParseError('Not a Header')

        if isinstance(ftnSta, File):
            self.fnam = ftnSta.getCurrentFile()
            self.lbof = ftnSta.getStatementStartLine()
        else:
            self.fnam = None
            self.lbof = -1
        self.leof = -1

        self.empty = True
        self.sommaire = ''
        self.description = ''
        self.entrees  = ''
        self.sorties = ''
        self.notes = ''
        self.interface = ''

        self.__parse(ftnSta)
        if self.sommaire    != '': self.empty = False
        if self.description != '': self.empty = False
        if self.entrees     != '': self.empty = False
        if self.sorties     != '': self.empty = False
        if self.notes       != '': self.empty = False
        if self.interface   != '': self.empty = False

    def __parse(self, s):
        blkActif = ''
        while next(s).isInCmtBlk():
            #print s.getAsTxt()()
            l = s.getAsTxt().strip()
            if l == '':
                blkActif = ''
            elif l.startswith('Sommaire:'):
                blkActif = 'sommaire'
                l = l[9:].strip()
            elif l.startswith('Description:'):
                blkActif = 'description'
                l = l[12:].strip()
            elif l.startswith('Entrée:'):
                blkActif = 'entrees'
                l = l[7:].strip()
            elif l.startswith('Sortie:'):
                blkActif = 'sorties'
                l = l[7:].strip()
            elif l.startswith('Notes:'):
                blkActif = 'notes'
                l = l[6:].strip()
            elif l.startswith('Interface:'):
                blkActif = 'interface'
                l = l[10:].strip()

            if blkActif:
                if self.__dict__[blkActif]: self.__dict__[blkActif] += ' '
                self.__dict__[blkActif] += l

        if isinstance(s, File):
            self.leof = s.getCurrentLine()
        return

    def getLineFirst(self):
        return self.lbof

    def getLineLast(self):
        return self.leof

class Comment:
    def __init__(self, sta):
        self.cmt = ''
        self.__parse(sta)

    def __parse(self, sta):
        LOGGER.debug('Cmt.parseCmt(%s)', str(sta))

        if isinstance(sta, FtnIf): return
        if sta.getAsTxt()[0:9] != '<comment>': return

        tag = TagString(sta)
        self.cmt = tag.getTxt()

    def getComment(self):
        return self.cmt

class Variable:
    def __init__(self, name, vtyp=None, visibility=Visibility.Private, desc=''):
        self.name = name
        self.vtyp = vtyp
        self.visibility = visibility
        self.desc = desc

    def setVisibility(self, v):
        self.visibility = v

    def getVisibility(self):
        return self.visibility

    def getType(self):
        return self.vtyp

    def getDescription(self):
        return self.desc

    def getName(self):
        return self.name

    def getDecl(self):
        return "%s %s" % (self.vtyp, self.name)

    def getStatement(self):
        return Statement(self.getDecl())

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getName()

    def __eq__(self, other):
        return self.name == other.name

class Declaration:
    def __init__(self, ftnSta, desc='', visibility=Visibility.Public):
        if not ftnSta.isDeclaration():
            raise FortranParseError('Not a Declaration')

        dcl = ftnSta.getAsTxt().strip()
        dtyp, tvis, names = self.__parseDecl(dcl)

        self.stmt = ftnSta
        self.desc = desc
        self.visibility = tvis if tvis else visibility
        self.dtyp = dtyp
        self.vars = []
        for n in names:
            self.vars.append(Variable(n, vtyp=self.dtyp, desc=self.desc))

    def __parseDecl(self, dcl):
        dcl = dcl.lstrip()
        tks = dcl.split('::')
        if len(tks) != 2:
            tks = dcl.split(None, 1)
            if len(tks) != 2:
                tks = dcl.split(')')
        typ = tks[0].strip()
        dcl = tks[1].strip()

        if dcl[0] == '*':
            typ += dcl[0]
            dcl = dcl[1:]
            dcl = dcl.lstrip()
            if dcl[0] == '(':
                typ += dcl[0:dcl.index(')')]
                dcl = dcl[dcl.index(')'):]
            else:
                typ += dcl[0:dcl.index(' ')]
                dcl = dcl[dcl.index(' '):]
        nms = [tk.strip() for tk in dcl.split(',')]

        vis = None
        if 'PUBLIC'  in typ: vis = Visibility.Public
        if 'PRIVATE' in typ: vis = Visibility.Private

        return typ, vis, nms

    def getType(self):
        return self.dtyp

    def getDescription(self):
        return self.desc

    def getVariables(self):
        return self.vars

    def getStatement(self):
        return self.stmt

    def getDecl(self):
        return "%s %s" % (self.getType(), self.getVariables())

    def __str__(self):
        return sansAccents( self.getDecl() )

    def __repr__(self):
        return self.getDecl()

class FtnIf:
    def __init__(self, decl, sta):
        LOGGER.debug('FtnIf.__init__: %s', decl)
        self.test = ''
        self.blks = []
        self.__parse(decl, sta)

    def __parse(self, decl, sta):
        LOGGER.debug('FtnIf.__parse: %s', decl)
        test = self.__parseDecl(decl)
        if decl[-4:] == 'THEN':
            self.__readBlock(sta, 'IF', test)
        else:
            decl = decl[decl.find(test)+len(test)+1:]
            decl = decl.strip()
            self.blks.append(('IF', test, [ Statement(decl) ]))
        self.test = test

    def __parseDecl(self, decl):
        LOGGER.debug('FtnIf.__parseDecl: %s', decl)
        decl = decl.strip()
        dc = decl.replace(' ', '')
        assert(dc[0:3] == 'IF(' or dc[0:7] == 'ELSEIF(')
        t = String(decl).getBlock('(', ')')
        return t

    def __readBlock(self, sta, ftyp, test=''):
        LOGGER.debug('FtnIf.__readBlock: %s', ftyp)

        blk = []
        self.blks.append((ftyp, test, blk))
        done = False
        while not done:
            s = next(sta)
            st = s.getAsTxt().strip()
            sc = st.replace(' ', '')
            if sc == 'ENDIF':
                done = True
            elif sc[0:3] == 'IF(':
                if_ = FtnIf(st, sta)
                blk.append(if_)
            elif sc[0:7] == 'ELSEIF(':
                t = self.__parseDecl(st)
                self.__readBlock(sta, 'ELSEIF', t)
                done = True
            elif sc == 'ELSE':
                self.__readBlock(sta, 'ELSE')
                done = True
            else:
                LOGGER.debug('FtnIf.__readBlock: append to block %s', str(s))
                blk.append( Statement(str(s)) )

        LOGGER.debug('FtnIf.__readBlock done')
        return

    def __iter__(self):
        return self.blks.__iter__()

    #def __next__(self):
    #    return self.blks.__next__()

    def __str__(self):
        return sansAccents( self.getAsTxt() )

    def getAsTxt(self):
        return self.test

    def p(self):
        for b in self:
            print('%s (%s)' % (b[0], b[1]))
            for s in b[2]:
                print('\t%s' % s)

class Common:
    def __init__(self, ftnSta, hdr=None, glbs=None):
        if not ftnSta.isCommon():
            raise FortranParseError('Not a Common')

        self.visibility = Visibility.Private

        dcl = ftnSta.getAsTxt().strip()
        name, vbls = self.__parseDecl(dcl, glbs)

        self.stmt = ftnSta
        self.hdr  = hdr
        self.name = name
        self.vbls = vbls

    def __parseDecl(self, dcl, glbs):
        dcl = dcl[7:]
        dcl = dcl.lstrip()
        dcl = dcl[1:]
        nom = dcl[0:dcl.index('/')]
        dcl = dcl[dcl.index('/')+1:]
        nom = nom.strip()
        dcl = dcl.strip()
        dcl += ','

        xprVar = r'([a-zA-Z_][a-zA-Z0-9_\$]*)'
        xprInt = r'([0-9]+)'
        xprCmp = r'(' + xprVar + r'|' + xprInt + r')'
        xprPar = r'(' + xprCmp + r'(\ *,\ *' + xprCmp + r')*' + r')'
        xprUne = r'((?P<var>' + xprVar + r')(\ *\(' + xprPar + r'\ *\))?' + r'\ *,)'
        patt = re.compile(xprUne)

        vbls = []
        if glbs:
            for m in patt.finditer(dcl):
                v = m.group('var')
                t = glbs.get(v)
                if t:
                    t.setVisibility = self.visibility
                    vbls.append(t)
        else:
            for m in patt.finditer(dcl):
                v = m.group('var')
                vbls.append(Variable(v, vtyp=None, visibility=self.visibility))

        return nom, vbls

    def getVars(self):
        return self.vbls

    def getName(self):
        return self.name

    def getStatement(self):
        return self.stmt

    def getDecl(self):
        return "/%s/%s" % (self.getName(), self.getVars())

    def getH2D2Class(self):
        tks = self.getName().split('_')
        if len(tks) > 1:
            return '_'.join(tks[0:2])
        return None

    def getH2D2Module(self):
        tks = self.getName().split('_')
        if len(tks) > 0:
            return tks[0]
        return None

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getDecl()

class Function:
    SUB_ITEMS = { }

    def __init__(self, ftnSta, hdr=None, doParseCode=True):
        dcl = ftnSta.getAsTxt().strip()
        if dcl[-1] != ')':
            raise FortranParseError('Not a valid function statement:\n%s' % dcl)

        if isinstance(ftnSta, File):
            self.fnam = ftnSta.getCurrentFile()
            if hdr:
                self.lbof = hdr.getLineFirst()
            else:
                self.lbof = ftnSta.getStatementStartLine()
        else:
            self.fnam = None
            self.lbof = -1
        self.leof = self.lbof

        ftype, fname, fargs, frsl = self.parseDecl(dcl)
        fkind = self.parseKind(fname)

        self.stmt = ftnSta
        self.hdr  = hdr
        self.ftyp = ftype
        self.name = fname
        self.frsl = frsl 
        self.args = fargs
        self.visibility = Visibility.Private
        self.kind = fkind
        self.pythonName = None
        self.subFnc = []
        self.subItf = []

        if doParseCode:
            self.parseCode(ftnSta)

    def parseKind(self, name):
        s = name.split('_')[-1]
        for e in MethodKind:
            if e.name == s:
                return e
        return MethodKind.General

    def parseDecl(self, fdcl):
        dcl = ' ' + fdcl
        if   ' FUNCTION '  in dcl:
            idx = dcl.index(' FUNCTION ')       # type FUNCTION name (args) ...
            typ = dcl[0:idx].strip()            # type
            dcl = dcl[idx+10:].strip()          # name (args) ...
            nme = dcl[0:dcl.index('(')].strip() # name
            dcl = dcl[dcl.index('('):].strip()  # (args) ...
            rsl = dcl[dcl.index(')')+1:].strip()# ...
            dcl = dcl[:dcl.index(')')+1].strip()# (args)
            idx = rsl.find('RESULT')
            rsl = rsl[idx+6:].strip() if idx >= 0 else ''
            rsl = rsl[1:rsl.index(')')].strip()  if rsl else ''
        elif ' SUBROUTINE ' in dcl:
            idx = dcl.index(' SUBROUTINE ')     # SUBROUTINE name (args) ...
            typ = ''                            # SUBROUTINE
            dcl = dcl[idx+12:].strip()          # name (args) ...
            nme = dcl[0:dcl.index('(')].strip() # name
            dcl = dcl[dcl.index('('):].strip()  # (args) ...
            dcl = dcl[:dcl.index(')')+1].strip()# (args)
            rsl = ''
        else:
            raise FortranParseError('Not a valid function statement:\n%s' % fdcl)

        dcl = dcl[1:-1]
        args = [ ]
        if len(dcl):
            args = [ Variable(tk.strip()) for tk in dcl.split(',') ]
        return typ, nme, args, rsl

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (1, 2, 3):  return False
        if tks[0] != 'END':          return False
        if len(tks) >  1 and tks[1] != 'FUNCTION': return False
        if len(tks) == 3 and tks[2] != self.name:  return False
        return True

    def parseCode(self, s):
        LOGGER.debug('Function.parseCode: %s begin', self.name)

        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = False
        s.doInclude  = True     # Function type is in an include
        s1 = s
        while not self.isEndOfCode(s1.getAsTxt()):
            s1 = next(s)
            l  = s1.getAsTxt().strip()
            c  = l.replace(' ', '').upper()
            if c[0:24] in ('CDEC$ATTRIBUTESDLLEXPORT', '!DEC$ATTRIBUTESDLLEXPORT') or \
               c[0:15] in ('!H2D2_DLLEXPORT', ):
                self.visibility = Visibility.Public
            elif s1.isComment():
                pass
            elif s1.isInterface():
                itf = Function.SUB_ITEMS['Interface'](s1)
                self.subItf.append(itf)
            elif s1.isFunction():
                f = Function.SUB_ITEMS['Function'](s1)
                self.subFnc.append(f)
            elif s1.isDeclaration():
                dcl = Function.SUB_ITEMS['Declaration'](s1)
                for v in dcl.getVariables():
                    try:
                        i = self.args.index(v)
                        self.args[i] = v
                    except Exception:
                        pass
                for v in dcl.getVariables():
                    if v.getName() == self.getName():
                        self.ftyp = v.getType()
                    if v.getName() == self.frsl:
                        self.ftyp = v.getType()
            elif s1.isTagPython():
                t = Function.SUB_ITEMS['TagString'](s1)
                self.pythonName = t.getTxt()
            else:
                self.parseStatement(s1)

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude
        s.clearTxt()
        LOGGER.debug('Function.parseCode: %s done', self.name)

    def parseStatement(self, s):
        if isinstance(s, File):
            self.leof = s.getCurrentLine()

    def skipCode(self, s):

        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = True
        s.doInclude  = False
        while not self.isEndOfCode(s.getAsTxt()):
            s = next(s)
        self.leof = s.getCurrentLine()

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude

    def getArgs(self):
        res = [ v.getName() for v in self.args ]
        return ', '.join(res)

    def getKind(self):
        return self.kind

    def getFileName(self):
        return self.fnam

    def getLineFirst(self):
        return self.lbof

    def getLineLast(self):
        return self.leof

    def getName(self):
        return self.name

    def getType(self):
        return self.ftyp

    def getHeader(self):
        return self.hdr

    def getStatement(self):
        return self.stmt

    def getVisibility(self):
        return self.visibility

    def getDecl(self):
        return "%s %s(%s)" % (self.getType(), self.getName(), self.getArgs())

    def getH2D2Class(self):
        tks = self.getName().split('_')
        if   len(tks) > 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:3])
        elif len(tks) > 2 and len(tks[0]) in [2, 3, 4]:
            return '_'.join(tks[0:2])
        return None

    def getH2D2Module(self):
        tks = self.getName().split('_')
        if   len(tks) > 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:2])
        elif len(tks) > 2 and len(tks[0]) in [2, 3, 4]:
            return tks[0]
        return None

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getDecl()

class Subroutine(Function):
    def __init__(self, ftnSta, hdr=None, doParseCode=True):
        Function.__init__(self, ftnSta, hdr, doParseCode)

    def getDecl(self):
        return "SUBROUTINE %s(%s)" % (self.getName(), self.getArgs())

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (1,2,3):  return False
        if tks[0] != 'END':         return False
        if len(tks) >  1 and tks[1] != 'SUBROUTINE': return False
        if len(tks) == 3 and tks[2] != self.name: return False
        return True

class Procedure:
    def __init__(self, prcSta, desc='', visibility=Visibility.Public):
        if not prcSta.isProcedure():
            raise FortranParseError('Not a Procedure')

        dcl = prcSta.getAsTxt().strip()
        ptyp, tvis, names = self.__parseDecl(dcl)

        self.stmt = prcSta
        self.desc = desc
        self.visibility = tvis if tvis else visibility
        self.ptyp = ptyp
        self.vars = [ Variable(n, vtyp=self.ptyp, desc=self.desc) for n in names ]

    def __parseDecl(self, dcl):
        dcl = dcl.lstrip()
        tks = dcl.split('=>')
        if len(tks) != 2:
            tks = dcl.split('::')
            if len(tks) != 2:
                tks = dcl.rsplit(None, 1)
        typ = tks[0].strip()
        nms = [tk.strip() for tk in tks[1].split(',')]
        vis = None
        if 'PUBLIC'  in typ: vis = Visibility.Public
        if 'PRIVATE' in typ: vis = Visibility.Private

        return typ, vis, nms

    def getType(self):
        return self.ptyp

    def getDescription(self):
        return self.desc

    def getVariables(self):
        return self.vars

    def getStatement(self):
        return self.stmt

    def getDecl(self):
        return "%s %s" % (self.getType(), self.getVariables())

    def __str__(self):
        return sansAccents( self.getDecl() )

    def __repr__(self):
        return self.getDecl()

class Type:
    SUB_ITEMS = { }
    def __init__(self, typSta, visibility=Visibility.Public, hdr=None, doParseCode=True):
        dcl = typSta.getAsTxt().strip()

        if isinstance(typSta, File):
            self.fnam = typSta.getCurrentFile()
            if hdr:
                self.lbof = hdr.getLineFirst()
            else:
                self.lbof = typSta.getStatementStartLine()
        else:
            self.fnam = None
            self.lbof = -1
        self.leof = self.lbof

        tname, tvis = self.parseDecl(dcl)

        self.stmt = typSta
        self.hdr  = hdr
        self.visibility = tvis if tvis else visibility
        self.name = tname
        self.pythonName = None
        self.attributs = []
        self.procedures= []
        self.methods   = []

        if doParseCode:
            self.parseCode(typSta)

    def parseDecl(self, tdcl):
        dcl = tdcl.strip()
        if  dcl[0:5] in ('TYPE ', 'TYPE(', 'TYPE,'):
            tks = dcl.split('::')
            if len(tks) < 2:
                tks = dcl.split()
            typ = tks[:-1]
            nme = tks[-1].strip()
            vis = None
            if 'PUBLIC'  in typ: vis = Visibility.Public
            if 'PRIVATE' in typ: vis = Visibility.Private
        else:
            raise FortranParseError('Not a valid type statement:\n%s' % tdcl)
        return nme, vis

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (2,3): return False
        if tks[0] != 'END':  return False
        if tks[1] != 'TYPE': return False
        if len(tks) == 3 and tks[2] != self.name: return False
        return True

    def parseCode(self, s):
        LOGGER.debug('Type.parseCode %s begin', self.name)

        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = False
        s.doInclude  = True
        s1 = s
        while not self.isEndOfCode(s1.getAsTxt()):
            s1 = next(s)
            if s1.isDeclaration():
                a = Type.SUB_ITEMS['Declaration'](s1, visibility=self.visibility)
                self.attributs.append(a)
            elif s1.isProcedure():
                p = Type.SUB_ITEMS['Procedure'](s1, visibility=self.visibility)
                self.procedures.append(p)
            else:
                self.parseStatement(s1)

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude
        s.clearTxt()
        LOGGER.debug('Type.parseCode %s done', self.name)

    def parseModuleBegin(self):
        pass

    def parseModuleEnd(self):
        pass

    def parseStatement(self, s):
        if isinstance(s, File):
            self.leof = s.getCurrentLine()

    def skipCode(self, s):
        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = True
        s.doInclude  = False
        while not self.isEndOfCode(s.getAsTxt()):
            s = next(s)
        self.leof = s.getCurrentLine()

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude

    def getFileName(self):
        return self.fnam

    def getLineFirst(self):
        return self.lbof

    def getLineLast(self):
        return self.leof

    def getName(self):
        return self.name

    def getHeader(self):
        return self.hdr

    def getStatement(self):
        return self.stmt

    def getAttributs(self):
        return self.attributs

    def getMethods(self):
        return self.methods

    def getH2D2Class(self):
        tks = self.getName().split('_')
        if   len(tks) > 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:3])
        elif len(tks) > 2 and len(tks[0]) in [2, 3, 4]:
            return '_'.join(tks[0:2])
        return None

    def getH2D2Module(self):
        tks = self.getName().split('_')
        if   len(tks) > 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:2])
        elif len(tks) > 2 and len(tks[0]) in [2, 3, 4]:
            return tks[0]
        return None

    def addMethod(self, f):
        vp = Variable(f.name)
        for p in self.procedures:
            if vp in p.vars:
                f.visibility = p.visibility
                break
        self.methods.append(f)

    def hasProcedure(self, pname):
        vp = Variable(pname)
        for p in self.procedures:
            if vp in p.vars: return True
        return False

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getName()

class Interface:
    SUB_ITEMS = { }

    def __init__(self, itfSta, visibility=Visibility.Public, doParseCode = True):
        dcl = itfSta.getAsTxt().strip()

        if isinstance(itfSta, File):
            self.fnam = itfSta.getCurrentFile()
            self.lbof = itfSta.getStatementStartLine()
        else:
            self.fnam = None
            self.lbof = -1
        self.leof = self.lbof

        iname = self.parseDecl(dcl)

        self.stmt  = itfSta
        self.visibility = visibility
        self.name  = iname
        self.procedures = []
        self.funcs = []

        if doParseCode:
            self.parseCode(itfSta)

    def parseDecl(self, idcl):
        dcl = idcl.strip().split()
        try:
            i = dcl.index('INTERFACE')
            nme = dcl[i+1].strip() if (i+1) < len(dcl) else ''
        except Exception:
            raise FortranParseError('Not a valid interface statement:\n%s' % idcl)
        return nme

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (2,3):  return False
        if tks[0] != 'END':       return False
        if tks[1] != 'INTERFACE': return False
        if len(tks) == 3 and tks[2] != self.name: return False
        return True

    def parseCode(self, s):
        LOGGER.debug('Interface.parseCode %s begin', self.name)

        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = False
        s.doInclude  = True
        s1 = s
        while not self.isEndOfCode(s1.getAsTxt()):
            s1 = next(s)
            if (s1.isFunction()):
                f = Interface.SUB_ITEMS['Function'](s1)
                self.funcs.append(f)
            elif (s1.isProcedure()):
                p = Interface.SUB_ITEMS['Procedure'](s1, visibility=self.visibility)
                self.procedures.append(p)
            else:
                self.parseStatement(s1)

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude
        s.clearTxt()
        LOGGER.debug('Interface.parseCode %s done', self.name)

    def parseInterfaceBegin(self):
        pass

    def parseInterfaceEnd(self):
        pass

    def parseStatement(self, s):
        if isinstance(s, File):
            self.leof = s.getCurrentLine()

    def skipCode(self, s):
        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = True
        s.doInclude  = False
        while not self.isEndOfCode(s.getAsTxt()):
            s = next(s)
        self.leof = s.getCurrentLine()

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude

    def getFileName(self):
        return self.fnam

    def getLineFirst(self):
        return self.lbof

    def getLineLast(self):
        return self.leof

    def getName(self):
        return self.name if self.name else 'NoNameInterface'

    def getStatement(self):
        return self.stmt

    def addFunction(self, f):
        vp = Variable(f.name)
        for p in self.procedures:
            if vp in p.vars:
                f.visibility = p.visibility
                break
        self.funcs.append(f)

    def hasProcedure(self, pname):
        vp = Variable(pname)
        for p in self.procedures:
            if vp in p.vars: return True
        return False

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getName()

class Module:
    SUB_ITEMS = { }

    def __init__(self, mdlSta, hdr = None, doParseCode = True):

        if isinstance(mdlSta, File):
            self.fnam = mdlSta.getCurrentFile()
            if hdr:
                self.lbof = hdr.getLineFirst()
            else:
                self.lbof = mdlSta.getStatementStartLine()
        else:
            self.fnam = None
            self.lbof = -1
        self.leof = self.lbof

        dcl = mdlSta.getAsTxt().strip()
        mname = self.parseDecl(dcl)

        self.stmt = mdlSta
        self.hdr  = hdr
        self.name = mname
        self.pythonName = None
        self.funcs = []
        self.types = []
        self.ifaces= []

        if (doParseCode):
            self.parseCode(mdlSta)

    def parseDecl(self, mdcl):
        dcl = mdcl.strip()
        if  dcl[0:7] == 'MODULE ':
            nme = dcl[7:].strip()
        else:
            raise FortranParseError('Not a valid module statement:\n%s' % mdcl)
        return nme

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (2,3): return False
        if tks[0] != 'END':      return False
        if tks[1] != 'MODULE':   return False
        if len(tks) == 3 and tks[2] != self.name: return False
        return True

    def parseCode(self, s):
        LOGGER.debug('Module.parseCode %s begin', self.name)

        curVis = Visibility.Public
        curHdr = None
        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = True
        s.doInclude  = True
        s1 = s
        while not self.isEndOfCode(s1.getAsTxt()):
            s1 = next(s)
            if s.isHeader():
                h = Header(s)
                curHdr = h
            elif s1.isVisibility():
                v = Visibility.Public if 'PUBLIC' in s1.getAsTxt() else Visibility.Private
                curVis = v
            elif s1.isType():
                t = Module.SUB_ITEMS['Type'](s1, visibility=curVis, hdr=curHdr)
                curHdr = None
                self.types.append(t)
            elif s1.isInterface():
                i = Module.SUB_ITEMS['Interface'](s1, visibility=curVis)
                self.ifaces.append(i)
            elif s1.isFunction():
                f = Module.SUB_ITEMS['Function'](s1, hdr=curHdr)
                curHdr = None
                if f:
                    for t in self.types:
                        if t.hasProcedure(f.getName()):
                            t.addMethod(f)
                            f = None
                            break
                if f:
                    for i in self.ifaces:
                        if i.hasProcedure(f.getName()):
                            i.addFunction(f)
                            f = None
                            break
                if f: 
                    self.funcs.append(f)
            elif s1.isSubroutine():
                f = Module.SUB_ITEMS['Subroutine'](s1, hdr=curHdr)
                curHdr = None
                if f:
                    for t in self.types:
                        if t.hasProcedure(f.getName()):
                            t.addMethod(f)
                            f = None
                            break
                if f:
                    for i in self.ifaces:
                        if i.hasProcedure(f.getName()):
                            i.addFunction(f)
                            f = None
                            break
                if f: 
                    self.funcs.append(f)
            else:
                self.parseStatement(s1)

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude
        s.clearTxt()
        LOGGER.debug('Module.parseCode %s done', self.name)

    def parseModuleBegin(self):
        pass

    def parseModuleEnd(self):
        pass

    def parseStatement(self, s):
        if isinstance(s, File):
            self.leof = s.getCurrentLine()

    def skipCode(self, s):
        doStripCmt = s.doStripCmt
        doInclude  = s.doInclude
        s.doStripCmt = True
        s.doInclude  = False
        while not self.isEndOfCode(s.getAsTxt()):
            s = next(s)
        self.leof = s.getCurrentLine()

        s.doStripCmt = doStripCmt
        s.doInclude  = doInclude

    def getFileName(self):
        return self.fnam

    def getLineFirst(self):
        return self.lbof

    def getLineLast(self):
        return self.leof

    def getName(self):
        return self.name

    def getHeader(self):
        return self.hdr

    def getStatement(self):
        return self.stmt

    def getFunctions(self):
        return self.funcs

    def getH2D2Class(self):
        tks = self.getName().split('_')
        if len(tks) > 0 and tks[-1] == 'M':
            del tks[-1]
        if   len(tks) >= 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:3])
        elif len(tks) >= 2 and len(tks[0]) in [2, 3, 4]:
            return '_'.join(tks[0:2])
        elif len(tks) == 1:
            return tks[0]
        return None

    def getH2D2Module(self):
        tks = self.getName().split('_')
        if len(tks) > 0 and tks[-1] == 'M':
            del tks[-1]
        if   len(tks) >= 3 and tks[0] == 'IC' and len(tks[1]) == 2:
            return '_'.join(tks[0:2])
        elif len(tks) >= 2 and len(tks[0]) in [2, 3, 4]:
            return tks[0]
        elif len(tks) == 1:
            return tks[0]
        return None

    def __str__(self):
        return sansAccents( self.getName() )

    def __repr__(self):
        return self.getName()

class SubModule(Module):
    def __init__(self, mdlSta, hdr = None, doParseCode = True):
        super(SubModule, self).__init__(mdlSta, hdr = hdr, doParseCode = doParseCode)

    def parseDecl(self, mdcl):
        dcl = mdcl.strip()
        if  dcl[0:9] == 'SUBMODULE':
            dcl = dcl[9:].strip()
            if dcl[0] == '(':
                dcl, nam = dcl.split(')')
                dcl = dcl[1:].strip()
                nme = nam.strip()
            else:
                nme = dcl
        else:
            raise FortranParseError('Not a valid submodule statement:\n"%s"' % mdcl)
        return nme

    def isEndOfCode(self, s):
        tks = s.split()
        if len(tks) not in (2,3):   return False
        if tks[0] != 'END':        return False
        if tks[1] != 'SUBMODULE':  return False
        if len(tks) == 3 and tks[2] != self.name: return False
        return True

Function.SUB_ITEMS = {
    'Declaration': Declaration,
    'Interface':   Interface,
    'Function':    Function,
    'TagString':   TagString,
    }
Type.SUB_ITEMS = {
    'Visibility':  Visibility,
    'Declaration': Declaration,
    'Procedure':   Procedure,
    }
Interface.SUB_ITEMS = {
    'Function'  :  Function,
    'Procedure':   Procedure,
    }
Module.SUB_ITEMS = {
    'Visibility': Visibility,
    'Type'      : Type,
    'Interface' : Interface,
    'Function'  : Function,
    'Subroutine': Subroutine,
    }
