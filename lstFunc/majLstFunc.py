#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import hashlib
import logging
import os
import sys
import win32clipboard as w

try:
    HOME = os.environ['INRS_DEV']
    if os.path.isdir(HOME):
        xtrDir = os.path.join(HOME, 'toolbox/xtrapi')
        if os.path.isdir(xtrDir):
            sys.path.append(xtrDir)
        else:
            raise RuntimeError('Not a valid directory: %s' % xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s' % os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrapi

LOGGER = logging.getLogger("INRS.IEHSS.Fortran")

def xeqRecursion(pth):
    if os.path.isdir(pth):
        for f in os.listdir(pth):
            fullPath = os.path.join(pth, f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                xeqRecursion(fullPath)
            elif os.path.splitext(fullPath)[1].lower() in ['.for', '.f90']:
                xeqAction(fullPath)
    else:
        xeqAction(pth)

def xeqAction(pth):
    inp = pth
    tmp = '.'.join( [inp, 'new'] )          # Add new extension
    bck = '.'.join( [inp, 'bak'] )          # Backup

    xeqOneFile(inp, tmp)

    if os.path.isfile(inp):
        if os.path.isfile(tmp):
            inp_fic = open(inp, 'rb')
            tmp_fic = open(tmp, 'rb')
            inp_md5 = hashlib.md5( inp_fic.read() )
            tmp_md5 = hashlib.md5( tmp_fic.read() )
            inp_fic.close()
            tmp_fic.close()
            if tmp_md5.digest() != inp_md5.digest():
                if os.path.isfile(bck): os.remove(bck)
                os.renames(inp, bck)
                os.renames(tmp, inp)
                LOGGER.info(' --> Updating %s', inp)
            else:
                os.remove(tmp)
        else:
            os.renames(tmp, inp)
            LOGGER.info(' --> Creating %s', inp)

def getFunctionsList(inp):
    lst = [ '! Interface:' ]

    tmp = '.'.join( [inp, 'lst'] )
    inc = os.path.join(HOME, 'H2D2/h2d2.i')

#    try:
    if True:
        args = '@%s --format decl --extract-public --extract-private --output %s %s' % (inc, tmp, inp)
        args = args.split(' ')
        xtrapi.main(args)
        with open(tmp, mode='r', encoding='utf-8') as ftmp:
            lst.extend( [l.rstrip() for l in ftmp.readlines()] )
#    except:
    else:
        pass

    try:
        os.remove(tmp)
    except OSError:
        pass

    return lst

def writeLineFOR(fout, line):
    fout.write("%s\n" % line)

def writeLineF90(fout, line):
    if line and line[0] == 'C':
        fout.write("!%s\n" % line[1:])
    else:    
        fout.write("%s\n" % line)

def writeBlk(fout, blk):
    for l in blk: writeLineFOR(fout, l)

def writeFncBlk(fout, lstFunc, lstInfo):
    try:
        grp = 'Fichier'
        for l in lstInfo[grp]: writeLineF90(fout, l)
        if lstInfo[grp][-1] not in ('C', '!'): writeLineF90(fout, 'C')
    except:
        pass

    try:
        grp = 'Sommaire'
        for l in lstInfo[grp]: writeLineF90(fout, l)
        if lstInfo[grp][-1] not in ('C', '!'): writeLineF90(fout, 'C')
    except:
        pass

    try:
        grp = 'Description'
        for l in lstInfo[grp]: writeLineF90(fout, l)
        if lstInfo[grp][-1] not in ('C', '!'): writeLineF90(fout, 'C')
    except:
        pass

    try:
        grp = 'Groupe'
        for l in lstInfo[grp]: writeLineF90(fout, l)
    except:
        pass
    try:
        grp = 'Sous-Groupe'
        for l in lstInfo[grp]: writeLineF90(fout, l)
        if lstInfo[grp][-1] not in ('C', '!'): writeLineF90(fout, 'C')
    except:
        pass
    try:
        grp = 'Note'
        for l in lstInfo[grp]: writeLineF90(fout, l)
        if lstInfo[grp][-1] not in ('C', '!'): writeLineF90(fout, 'C')
    except:
        pass

    for l in lstFunc:
        writeLineF90(fout, '%s' % l)
    writeLineF90(fout, '!')

def xeqOneFile(inp, out):
    lstFnc = getFunctionsList(inp)
    #for f in lstFnc:
    #    print(f)
    #raise

    finp = open(inp, mode='r', encoding='utf-8')
    fout = open(out, mode='w', encoding='utf-8')

    inBloc = False
    inFunc = False
    doWrite = True
    activeAcc = None
    blk    = []
    blkFnc = {}
    for l in finp.readlines():
        l = l.rstrip()

        doWrite= True
        if not l: 
            pass
        elif l[0] not in ('C', '!'):
            pass
        elif l[0] in ('C', '!') and l[1:25] == '************************':
            if inBloc:
                if inFunc:
                    writeFncBlk(fout, lstFnc, blkFnc)
                else:
                    writeBlk(fout, blk)
                inFunc = False
                blk     = []
                blkFunc = {}
                activeAcc = None
            inBloc = not inBloc

        elif inBloc:
            doWrite = False
            if l[0] in ('C', '!') and l[1:10] == ' Fichier:':
                inFunc = True
                activeAcc = 'Fichier'
            if l[0] in ('C', '!') and l[1:8] == ' Class:':
                inFunc = True
                activeAcc = None
            if l[0] in ('C', '!') and l[1:15] == ' Sousroutines:':
                inFunc = True
                activeAcc = None
            if l[0] in ('C', '!') and l[1:12] == ' Fonctions:':
                inFunc = True
                activeAcc = None
            if l[0] in ('C', '!') and l[1:12] == ' Functions:':
                inFunc = True
                activeAcc = None
            if l[0] in ('C', '!') and l[1:12] == ' Interface:':
                inFunc = True
                activeAcc = None

            #--- Gère l'accumulation des infos
            if l[0] in ('C', '!') and l[1:9] == ' Groupe:':
                activeAcc = 'Groupe'
            if l[0] in ('C', '!') and l[1:14] == ' Sous-Groupe:':
                activeAcc = 'Sous-Groupe'
            if l[0] in ('C', '!') and l[1:11] == ' Sommaire:':
                activeAcc = 'Sommaire'
            if l[0] in ('C', '!') and l[1:14] == ' Description:':
                activeAcc = 'Description'
            if l[0] in ('C', '!') and l[1:7] == ' Note:':
                activeAcc = 'Note'
            if l[0] in ('C', '!') and l[1:8] == ' Notes:':
                activeAcc = 'Note'
            if l[0] in ('C', '!') and l[1:25] == '************************':
                activeAcc = None
            if l.strip() in ('C', '!') and activeAcc != 'Note':
                activeAcc = None

            #---  Les erreurs
            if l.strip() == '':
                LOGGER.warning('')
                LOGGER.warning('Empty line in comment bloc:')
                for l in blk: LOGGER.warning('   ' + l[:-1])
                LOGGER.warning('')
                raise ValueError('Empty line in comment bloc')
            if l[0] == 'c':
                LOGGER.warning('')
                LOGGER.warning('Comment line with lowercase "c" in column 1 of comment bloc:')
                for l in blk: LOGGER.warning('   ' + l[:-1])
                LOGGER.warning('')
                raise ValueError('Comment line with lowercase "c" in column 1 of comment bloc')

        if doWrite:
            writeLineFOR(fout, l)
        elif inBloc:
            blk.append(l)
            if activeAcc:
                try:
                    blkFnc[activeAcc].append(l)
                except:
                    blkFnc[activeAcc] = [l]


def main(argv=None):
    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    if argv == None: argv = sys.argv[1:]
    if len(argv) == 0: argv.append('.')
    try:
        for a in argv:
            xeqRecursion(a)
    except Exception as e:
        LOGGER.error(str(e))

if __name__ == '__main__':
    #args = [ r'E:\inrs-dev\H2D2\h2d2_svc\source\sv\sosptr.f90' ]
    #main(args)
    main()
