//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: tpliste.h
//
// Classe:  TPListe
//
// Description:
//    Classe template de liste. Les éléments du vecteur sont stockés
//    par blocs.
//
// Attributs:
//
// Notes:
//
//************************************************************************
// 27-10-94  Yves Secretan      Version originale
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
//************************************************************************
#ifndef TPLISTE_H_DEJA_INCLU
#define TPLISTE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include "tpvector.h"

template <typename T> class TPListe : public TPVecteur<T>
{
public:
                 TPListe  (const EntierN = TPVecteur<T>::DIM_BLOC);
                 TPListe  (const TPListe&);
                 ~TPListe ();

   inline Entier efface (const EntierN);
          Entier effaceTout ();

protected:
   inline void invariant (ConstCarP) const;
};

//**************************************************************
// Description: Invariants de classe.
//
// Entrée:
//    ConstCarP conditionP
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 24 août 1993         par Yves Secretan
//**************************************************************
template <typename T> 
inline void TPListe<T>::invariant(ConstCarP conditionP) const
{
   TPVecteur<T>::invariant(conditionP);
}

#include "tpliste.hpp"

#endif   // define TPLISTE_H_DEJA_INCLU

