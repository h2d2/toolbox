//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette
// --- permission apparaissent dans toutes les copies ainsi que dans la
// --- documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: syallmem.hpp
// Classe:  SYAllocateurMemoire
//************************************************************************
// 27-10-94  Yves Secretan
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//**************************************************************

template <class TTData, EntierN dimBloc> const EntierN SYAllocateurMemoire<TTData, dimBloc>::DIM_BLOC = 16;

//**************************************************************
// Sommaire: Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>SYAllocateurMemoire()</code>
//    initialise les structures internes à la dimension du bloc
//    <code>dimBloc</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <class TTData, EntierN dimBloc>
SYAllocateurMemoire<TTData, dimBloc>::SYAllocateurMemoire ()
   : pool(dimBloc), pile(dimBloc)
{
#ifdef MODE_DEBUG
   PRECONDITION (dimBloc > 2);
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:   Destructeur
//
// Description:
//    Le destructeur public <code>~SYAllocateurMemoire()</code>
//    désalloue les structures internes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <class TTData, EntierN dimBloc>
SYAllocateurMemoire<TTData, dimBloc>::~SYAllocateurMemoire ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: Demande un pointeur.
//
// Description:
//    La méthode publique <code>demandePtr(...)</code> demande un
//    nouveau pointeur. Le pointeur est simplement pris de la pile
//    des pointeurs disponibles.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <class TTData, EntierN dimBloc>
TTData*
SYAllocateurMemoire<TTData, dimBloc>::demandePtr ()
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   EntierN ind;

   // ---  Augmente d'un bloc si la pile est vide
   if (pile.dimension() == 0)
   {
      TTData dum;
      ind = pool.dimension();
      for (EntierN i = 0; i < dimBloc; i++)
      {
         pool << dum;
         pile << &pool[ind++];
      }
   }

   // ---  Récupère le pointeur
   ind = pile.dimension() - 1;
   TTDataP tP = pile[ind];
   pile.enleve(ind);

#ifdef MODE_DEBUG
   POSTCONDITION (tP != NUL);
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return tP;
}

//**************************************************************
// Sommaire:   Relache un pointeur.
//
// Description:
//    La méthode publique <code>relachePtr(...)</code> relache le pointeur
//    passé en argument. Le pointeur est simplement ajouté à la liste
//    des pointeurs disponibles.
//
// Entrée:
//    const TTDataP tP;          Pointeur à relacher
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <class TTData, EntierN dimBloc>
void
SYAllocateurMemoire<TTData, dimBloc>::relachePtr (const TTDataP tP)
{
#ifdef MODE_DEBUG
   PRECONDITION (tP != NUL);
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Ajoute le pointeur à la pile
   pile.ajoute(tP);

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

