//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: tpvecsmp.hpp
// Classe:  TPVecteurSimple
//************************************************************************

#ifndef TPVECSMP_HPP_DEJA_INCLU
#define TPVECSMP_HPP_DEJA_INCLU

#include <cstring>

//**************************************************************
// Description: Constructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteurSimple<T>::TPVecteurSimple(const EntierN n)
   : TPVecteur<T> (n)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPVecteurSimple<T>::TPVecteurSimple (const EntierN n)


//**************************************************************
// Description: Copie Constructeur.
//
// Entrée:
//    const TPVecteur& t      : Vecteur à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteurSimple<T>::TPVecteurSimple (const TPVecteurSimple& t)
{
#ifdef MODE_DEBUG
   PRECONDITION (&t != NUL);
   t.invariant("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   TPVecteurSimple<T>& self = *this;
   
   // ---  Copie les attributs
   self.exposant = t.exposant;
   self.masque   = t.masque;
   self.dimBloc  = t.dimBloc;
   self.nbrBlocs = t.nbrBlocs;
   self.indBloc  = t.indBloc;
   self.indVect  = t.indVect;

   // ---  Dimensionne le vecteur de blocs
   self.blocsV = new typename TPVecteur<T>::TP[self.nbrBlocs];

   // ---  Crée les blocs
   for (Entier i = 0; i <= self.indBloc; i++)
      self.blocsV[i]= new T [self.dimBloc];

   // ---  Copie
   (*this) = t;

#ifdef MODE_DEBUG
   POSTCONDITION (self.blocsV != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPVecteurSimple<T>::TPVecteurSimple (const TPVecteurSimple& t)


//**************************************************************
// Description: Constructeur spécialisé.  Construit un vecteur de blocs
//    à partir d'un vecteur standard.
//
// Entrée:
//    const T*      tP     : Vecteur
//    const EntierN dim    : Dimension du vecteur
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteurSimple<T>::TPVecteurSimple(T* tP,
                                    const EntierN dim,
                                    const EntierN n)
   : TPVecteur<T>(n)
{
#ifdef MODE_DEBUG
   PRECONDITION (tP != NUL);
#endif   // ifdef MODE_DEBUG

   TPVecteurSimple<T>& self = *this;
   
   EntierN i, indT;
   size_t  nbytes;

   // ---  Crée le vecteur de blocs et les blocs
   self.nbrBlocs = dim / self.dimBloc;
   if ((self.nbrBlocs*self.dimBloc) < dim) self.nbrBlocs++;
   delete[] self.blocsV;
   self.blocsV = new typename TPVecteur<T>::TP[self.nbrBlocs];
   for (i = 0; i <= self.indBloc; i++)
      self.blocsV[i]= new T [self.dimBloc];

   // ---  Copie les éléments par copie de mémoire
   nbytes = self.dimBloc * sizeof(T);
   indT = 0;
   for (i = 0; i < self.indBloc; i++)
   {
      memcpy(self.blocsV[i], &tP[indT], nbytes);
      indT += self.dimBloc;
   }
   nbytes = (self.indVect+1) * sizeof(T);
   memcpy(self.blocsV[self.indBloc], &tP[indT], nbytes);

#ifdef MODE_DEBUG
   POSTCONDITION (self.blocsV    != NUL);
   POSTCONDITION (self.blocsV[0] != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPVecteurSimple<T>::TPVecteur (const T* tP,...)


//**************************************************************
// Description:
//    Destructeur. Désalloue les blocs puis le vecteur de blocs
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteurSimple<T>::~TPVecteurSimple()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}  // template <typename T> TPVecteurSimple<T>::~TPVecteurSimple()


//**************************************************************
// Description: operator =
//
// Entrée:
//    const TPVecteur t : TPVecteur à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteurSimple<T>& TPVecteurSimple<T>::operator = (const TPVecteurSimple& t)
{
#ifdef MODE_DEBUG
   PRECONDITION (&t != NUL);
   t.invariant("PRECONDITION");
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   TPVecteurSimple<T>& self = *this;
   
   // ---  Si les dimensions sont différentes, ajuste
   if (self.dimension() != t.dimension() || self.dimBloc != t.dimBloc)
   {
      // ---  Détruis les éléments actuels
      self.vide();
      delete[] self.blocsV[0];

      // ---  Ajuste le vecteur de blocs
      if (t.nbrBlocs > self.nbrBlocs)
      {
         delete[] self.blocsV;
         self.blocsV = new typename TPVecteur<T>::TP[t.nbrBlocs];
      }

      // ---  Copie les attributs
      self.exposant = t.exposant;
      self.masque   = t.masque;
      self.dimBloc  = t.dimBloc;
      self.nbrBlocs = t.nbrBlocs;
      self.indBloc  = t.indBloc;
      self.indVect  = t.indVect;

      // ---  Crée les blocs
      for (Entier i = 0; i <= self.indBloc; ++i)
         self.blocsV[i]= new T [self.dimBloc];
   }

   // ---  Copie les éléments par copie de mémoire
   size_t nbytes = self.dimBloc * sizeof(T);
   for (Entier i = 0; i < self.indBloc; i++)
      memcpy(self.blocsV[i], t.blocsV[i], nbytes);
   nbytes = (self.indVect+1) * sizeof(T);
   memcpy(self.blocsV[self.indBloc], t.blocsV[self.indBloc], nbytes);

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (*this);
}  // template <typename T> inline TPVecteurSimple<T>& TPVecteurSimple<T>::operator =

#endif   // TPVECSMP_HPP_DEJA_INCLU

