//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//******************************************************************************
// Fichier: prvers.h
//******************************************************************************
// 25-03-98  Yves Secretan  Version initiale
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//******************************************************************************

#ifndef PRVERS_H_DEJA_INCLU
#define PRVERS_H_DEJA_INCLU

extern char VERSION_PARSER[];

#endif   // PRVERS_H_DEJA_INCLU

