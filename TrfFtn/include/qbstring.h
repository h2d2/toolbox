//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: qbstring.h
//
// Classe:  QBString
//
// Sommaire:   Chaîne de caractères à la QuickBasic.
//
// Description:
//    La classe <code>QBString</code> est une implantation d'une chaîne de
//    caractères inspirée du QuickBasic. La chaîne n'est pas dynamique, sa
//    longueur maximale est donnée par l'attribut MAXSIZE.
//
// Attributs:
//
// Notes:
//
//************************************************************************
// 15-08-95  Yves Secretan
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//************************************************************************
#ifndef QBSTRING_H_DEJA_INCLU
#define QBSTRING_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include <cstddef>
#include <cstring>

DECLARE_CLASS(QBString);
#include <iosfwd>

Entier   instr  (const QBString&, ConstCarP);
Entier   PR_EXPORT_MODIF instr  (const QBString&, const QBString&);
Entier   PR_EXPORT_MODIF rinstr (const QBString&, ConstCarP);
Entier   PR_EXPORT_MODIF rinstr (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF delChar(const QBString&, char);
QBString PR_EXPORT_MODIF erase  (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF erase  (const QBString&, ConstCarP);
QBString PR_EXPORT_MODIF invert (const QBString&);
QBString PR_EXPORT_MODIF lcase  (const QBString&);
QBString PR_EXPORT_MODIF ucase  (const QBString&);
QBString PR_EXPORT_MODIF left   (const QBString&, const Entier);
QBString PR_EXPORT_MODIF left   (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF left   (const QBString&, ConstCarP);
QBString PR_EXPORT_MODIF right  (const QBString&, const Entier);
QBString PR_EXPORT_MODIF mid    (const QBString&, const Entier);
QBString PR_EXPORT_MODIF mid    (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF mid    (const QBString&, ConstCarP);
QBString PR_EXPORT_MODIF mid    (const QBString&, const Entier, const Entier);
QBString PR_EXPORT_MODIF bloc   (const QBString&, const QBString&, const QBString&);
QBString PR_EXPORT_MODIF bloc   (const QBString&, ConstCarP, ConstCarP);
QBString PR_EXPORT_MODIF rbloc  (const QBString&, const QBString&, const QBString&);
QBString PR_EXPORT_MODIF rbloc  (const QBString&, ConstCarP, ConstCarP);
QBString PR_EXPORT_MODIF subst  (const QBString&, const QBString&, const QBString&);
QBString PR_EXPORT_MODIF subst  (const QBString&, ConstCarP, ConstCarP);
QBString PR_EXPORT_MODIF token  (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF token  (const QBString&, ConstCarP);
QBString PR_EXPORT_MODIF rtoken (const QBString&, const QBString&);
QBString PR_EXPORT_MODIF rtoken (const QBString&, ConstCarP);
QBString PR_EXPORT_MODIF trim   (const QBString&);
QBString PR_EXPORT_MODIF ltrim  (const QBString&);
QBString PR_EXPORT_MODIF rtrim  (const QBString&);
QBString PR_EXPORT_MODIF operator +  (ConstCarP, const QBString&);
QBString PR_EXPORT_MODIF operator +  (Car, const QBString&);
std::istream& PR_EXPORT_MODIF operator >> (std::istream&, QBString&);
std::ostream& PR_EXPORT_MODIF operator << (std::ostream&, const QBString&);

class PR_EXPORT_MODIF QBString
{
public:
   enum {MAXSIZE = 1024};
   DECLARE_STRUCT(InnerString);
   struct InnerString
   {
      Car chaineP[MAXSIZE];
      void* operator new    (std::size_t);
      void  operator delete (void*);
   };
   
            QBString();
            QBString(ConstCarP);
            QBString(const wchar_t*);
            QBString(const QBString&);
           ~QBString();
           
   EntierN   hash     (EntierN) const;
   CarP      dupAsCarP() const;
   
   Entier    len    () const;
   Entier    instr  (ConstCarP) const;
   Entier    instr  (const QBString&) const;
   Entier    rinstr (ConstCarP) const;
   Entier    rinstr (const QBString&) const;
   
   const QBString& delChar(char);
   const QBString& erase  (ConstCarP);
   const QBString& erase  (const QBString&);
   const QBString& invert ();
   const QBString& lcase  ();
   const QBString& ucase  ();

   const QBString& left   (Entier);
   const QBString& left   (const QBString&);
   const QBString& left   (ConstCarP);
   const QBString& right  (Entier);
   const QBString& mid    (Entier);
   const QBString& mid    (const QBString&);
   const QBString& mid    (ConstCarP);
   const QBString& mid    (const Entier, const Entier);
   const QBString& bloc   (const QBString&, const QBString&);
   const QBString& bloc   (ConstCarP, ConstCarP);
   const QBString& rbloc  (const QBString&, const QBString&);
   const QBString& rbloc  (ConstCarP, ConstCarP);
   const QBString& subst  (const QBString&, const QBString&);
   const QBString& subst  (ConstCarP, ConstCarP);
   const QBString& token  (const QBString&);
   const QBString& token  (ConstCarP);
   const QBString& rtoken (const QBString&);
   const QBString& rtoken (ConstCarP);
   const QBString& trim   ();
   const QBString& ltrim  ();
   const QBString& rtrim  ();

   const QBString& operator =  (const QBString&);
   const QBString& operator =  (ConstCarP);
         QBString  operator +  (const QBString&) const;
         QBString  operator +  (ConstCarP) const;
         QBString  operator +  (Car) const;
   const QBString& operator += (const QBString&);
   const QBString& operator += (ConstCarP);
   const QBString& operator += (Car);

   Booleen   operator == (const QBString&) const;
   Booleen   operator == (ConstCarP) const;
   Booleen   operator == (CarP) const;
   Booleen   operator != (const QBString& s) const {return !(*this == s);}
   Booleen   operator != (ConstCarP cP     ) const {return !(*this == cP);}
   Booleen   operator != (CarP cP          ) const {return !(*this == cP);}
   Booleen   operator <  (const QBString& s) const {return  (*this < s.chaineP);}
   Booleen   operator <  (ConstCarP        ) const;
   Car       operator [] (EntierN i) const {return chaineP[i];};
   Car       operator [] (Entier  i) const {return chaineP[i];};
   Car       operator [] (int     i) const {return chaineP[i];};
             operator CarP () {return chaineP;}
             operator ConstCarP () const {return chaineP;}

   friend Entier   instr  (const QBString&, ConstCarP);
   friend Entier   PR_EXPORT_MODIF instr  (const QBString&, const QBString&);
   friend Entier   PR_EXPORT_MODIF rinstr (const QBString&, ConstCarP);
   friend Entier   PR_EXPORT_MODIF rinstr (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF delChar(const QBString&, char);
   friend QBString PR_EXPORT_MODIF erase  (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF erase  (const QBString&, ConstCarP);
   friend QBString PR_EXPORT_MODIF invert (const QBString&);
   friend QBString PR_EXPORT_MODIF lcase  (const QBString&);
   friend QBString PR_EXPORT_MODIF ucase  (const QBString&);
   friend QBString PR_EXPORT_MODIF left   (const QBString&, const Entier);
   friend QBString PR_EXPORT_MODIF left   (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF left   (const QBString&, ConstCarP);
   friend QBString PR_EXPORT_MODIF right  (const QBString&, const Entier);
   friend QBString PR_EXPORT_MODIF mid    (const QBString&, const Entier);
   friend QBString PR_EXPORT_MODIF mid    (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF mid    (const QBString&, ConstCarP);
   friend QBString PR_EXPORT_MODIF mid    (const QBString&, const Entier, const Entier);
   friend QBString PR_EXPORT_MODIF bloc   (const QBString&, const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF bloc   (const QBString&, ConstCarP, ConstCarP);
   friend QBString PR_EXPORT_MODIF rbloc  (const QBString&, const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF rbloc  (const QBString&, ConstCarP, ConstCarP);
   friend QBString PR_EXPORT_MODIF subst  (const QBString&, const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF subst  (const QBString&, ConstCarP, ConstCarP);
   friend QBString PR_EXPORT_MODIF token  (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF token  (const QBString&, ConstCarP);
   friend QBString PR_EXPORT_MODIF rtoken (const QBString&, const QBString&);
   friend QBString PR_EXPORT_MODIF rtoken (const QBString&, ConstCarP);
   friend QBString PR_EXPORT_MODIF trim   (const QBString&);
   friend QBString PR_EXPORT_MODIF ltrim  (const QBString&);
   friend QBString PR_EXPORT_MODIF rtrim  (const QBString&);
   friend QBString PR_EXPORT_MODIF operator +  (ConstCarP, const QBString&);
   friend QBString PR_EXPORT_MODIF operator +  (Car, const QBString&);
   friend std::istream& PR_EXPORT_MODIF operator >> (std::istream&, QBString&);
   friend std::ostream& PR_EXPORT_MODIF operator << (std::ostream&, const QBString&);

protected:
   void invariant (ConstCarP) const;

private:
   InnerStringP inStrP;
   CarP chaineP;
};

//************************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les invariants
//    de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITON" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void QBString::invariant(ConstCarP conditionP) const
{
   INVARIANT(strlen(chaineP) < MAXSIZE, conditionP);
   INVARIANT(CarN(chaineP[MAXSIZE-1]) == 0xAB, conditionP);
}
#else
inline void QBString::invariant(ConstCarP) const
{
}
#endif   // #ifdef MODE_DEBUG

#include "qbstring.hpp"

#endif // QBSTRING_H_DEJA_INCLU
