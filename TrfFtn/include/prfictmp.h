//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier: prfictmp.h
//
// Classe:  PRNomFichierTemporaire
//
// Sommaire:   Encapsule un nom de fichier temporaire
//
// Description:
//    La classe <code>PRNomFichierTemporaire</code> encapsule et gère un nom
//    de fichier temporaire. A la construction il génère un nom unique et Ó la
//    destruction il efface le fichier. On garanti ainsi que la ressource est
//    relachée même en cas d'exception.
//
// Attributs:
//    PRStatement nom;        Nom du fichier
//
// Notes:
//
//*****************************************************************************
// 01-11-99  Yves Secretan  Version Web++ 2.0b
//*****************************************************************************
#ifndef PRFICTMP_H_DEJA_INCLU
#define PRFICTMP_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include "prstat.h"

class PR_EXPORT_MODIF PRNomFichierTemporaire
{
public:
    PRNomFichierTemporaire     ();
   ~PRNomFichierTemporaire     ();
   operator const PRStatement& () const {return nom;}

private:
   PRStatement nom;
};

#endif   // PRFICTMP_H_DEJA_INCLU

