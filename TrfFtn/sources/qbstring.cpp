//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: qbstring.cpp
// Classe : QBString
//************************************************************************
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//************************************************************************
#include "qbstring.h"

#include "syallmem.h"
#include "tpobjdef.h"

#include <cstring>
#include <cstdlib>

TPObjetDefere<SYAllocateurMemoire<QBString::InnerString, 0x1 << 5> > alloc;

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline void* QBString::InnerString::operator new (std::size_t)
{
   return alloc()->demandePtr();
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline void QBString::InnerString::operator delete (void* tP)
{
   if (alloc() != NUL)
      alloc()->relachePtr(static_cast<QBString::InnerString*>(tP));
}

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description:
//    Le constructeur public <code>QBString()</code> initialise
//    l'objet comme une chaîne vide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString::QBString() : inStrP(NUL)
{
   using namespace std;

   inStrP = new InnerString;
   chaineP = inStrP->chaineP;
   memset(chaineP, 0x0, MAXSIZE-1);
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // QBString::QBString

//**************************************************************
// Sommaire: Constructeur
//
// Description:
//    Le constructeur public <code>QBString(ConstCarP)</code> initialise
//    l'objet comme copie de la chaîne passée en argument.
//
// Entrée:
//    ConstCarP copieP;       Chaîne à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString::QBString(ConstCarP copieP) : inStrP(NUL)
{
   using namespace std;

   if (strlen(copieP) >= MAXSIZE)
      throw;

   inStrP = new InnerString;
   chaineP = inStrP->chaineP;
   memset(chaineP, 0x0, MAXSIZE-1);
   if (copieP != NUL)
      strncpy (chaineP, copieP, MAXSIZE);
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // QBString::QBString(CarP)

//**************************************************************
// Sommaire: Constructeur
//
// Description:
//    Le constructeur public <code>QBString(ConstCarP)</code> initialise
//    l'objet comme copie de la chaîne passée en argument.
//
// Entrée:
//    ConstCarP copieP;       Chaîne à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString::QBString(const wchar_t* copieP) : inStrP(NUL)
{
   using namespace std;

   if (wcslen(copieP) >= MAXSIZE) throw;

   inStrP = new InnerString;
   chaineP = inStrP->chaineP;
   memset(chaineP, 0x0, MAXSIZE-1);
   if (copieP != NUL)
      wcstombs(chaineP, copieP, wcslen(copieP));
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // QBString::QBString(CarP)

//**************************************************************
// Sommaire:   Constructeur copie
//
// Description:
//    Le constructeur public <code>QBString(const QBString&)</code>
//    initialise l'objet comme copie de la chaîne passée en argument.
//
// Entrée:
//    const QBString& copie;     Chaîne à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString::QBString(const QBString& copie) : inStrP(NUL)
{
   using namespace std;

   inStrP = new InnerString;
   chaineP = inStrP->chaineP;
   memset(chaineP, 0x0, MAXSIZE-1);
   strcpy(chaineP, copie.chaineP);
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // QBString::QBString(CarP)

//**************************************************************
// Sommaire:   Destructeur.
//
// Description:
//    Le destructeur public <code>~QBString()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString::~QBString()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   delete inStrP;
   inStrP = NUL;
   chaineP = NUL;

}  // QBString::~QBString

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
EntierN QBString::hash(const EntierN base) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   const size_t dim = strlen(chaineP);
   EntierN h = 0;
   for (size_t i = 0; i < dim; i++)
      h = ((h << 6) + chaineP[i]) % base;     // h = (64*h + chaineP[i]) % base;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return h;
}

//**************************************************************
// Sommaire:
//
// Description:
//    La fonction friend <code>erase(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString erase(const QBString& copie, const QBString& s)
{
   QBString res = copie;
   return res.erase(s.chaineP);
}  // QBString::erase

QBString erase(const QBString& copie, ConstCarP cP)
{
   QBString res = copie;
   return res.erase(cP);
}  // QBString::erase


//**************************************************************
// Sommaire:   Inverse la chaîne
//
// Description:
//    La fonction friend <code>invert(...)</code> retourne la chaîne
//    la chaîne passée en argument inversée, donc avec le dernier
//    charactère en premier, etc...
//
// Entrée:
//    const QBString& copie;     Chaîne à inverser
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString invert(const QBString& copie)
{
   QBString res = copie;
   return res.invert();
}  // QBString::invert


//**************************************************************
// Sommaire:   Inverse la chaîne
//
// Description:
//    La méthode publique <code>invert()</code> inverse l'objet
//    courant. Elle retourne une référence à l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::invert()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   size_t taille = strlen(chaineP);
   if (taille < 2) return *this;
   char* c1P = chaineP;
   char* c2P = c1P + taille - 1;
   while (c1P < c2P)
   {
      char ctmp = *c1P;
      *c1P = *c2P;
      *c2P = ctmp;
      ++c1P, --c2P;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::invert


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString lcase(const QBString& copie)
{
   QBString res = copie;
   return res.lcase();
}  // QBString::lcase

QBString ucase(const QBString& copie)
{
   QBString res = copie;
   return res.ucase();
}  // QBString::ucase

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString left(const QBString& copie, const Entier posi)
{
   QBString res(copie);
   return res.left(posi);
}  // QBString::left

QBString left(const QBString& copie, ConstCarP cP)
{
   QBString res(copie);
   return res.left(cP);
}  // QBString::left

QBString left(const QBString& copie, const QBString& s)
{
   QBString res(copie);
   return res.left(s);
}  // QBString::left


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString right(const QBString& copie, const Entier taille)
{
   QBString res(copie);
   return res.right(taille);
}  // QBString::right


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString mid(const QBString& copie, const Entier posi)
{
   QBString res(copie);
   return res.mid(posi);
}  // QBString::mid

QBString mid(const QBString& copie, const QBString& s)
{
   QBString res(copie);
   return res.mid(s);
}  // QBString::mid

QBString mid(const QBString& copie, ConstCarP cP)
{
   QBString res(copie);
   return res.mid(cP);
}  // QBString::mid


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
Entier QBString::rinstr(ConstCarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   PRECONDITION(*cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   ConstCarP c1P = strstr(chaineP, cP);
   if (c1P != NULL)
   {
      ConstCarP c2P;
      while ( (c2P = strstr(c1P+1, cP)) != NULL)
         c1P = c2P;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (c1P == NULL) ? -1 : static_cast<Entier>(c1P-chaineP);
}  // QBString::rinstr


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::delChar(char c)
{
#ifdef MODE_DEBUG
   PRECONDITION(c != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   char *c1P = chaineP, *c2P = chaineP;
   while (*c1P)
   {
      if (*c1P != c)
         *c2P++ = *c1P;
      c1P++;
   }
   *c2P = 0x0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::delChar

QBString delChar(const QBString& copie, char c)
{
   QBString res = copie;
   return res.delChar(c);
}  // QBString::delChar


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString mid(const QBString& copie, const Entier pos, const Entier len)
{
   QBString res(copie);
   return res.mid(pos, len);
}  // QBString:mid


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::bloc(ConstCarP c1P, ConstCarP c2P)
{
#ifdef MODE_DEBUG
   PRECONDITION(c1P  != 0);
   PRECONDITION(*c1P != 0);
   PRECONDITION(c2P  != 0);
   PRECONDITION(*c2P != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   int ctr = 1;
   char *posP, *pos1P, *pos2P;

   pos1P = strstr(chaineP, c1P);
   pos2P = strstr(chaineP, c2P);
   if (pos1P == NUL || pos2P == NUL || pos2P < pos1P)
      return *this;

   posP = pos1P + 1;
   while (ctr != 0)
   {
      pos1P = strstr(posP, c1P);
      pos2P = strstr(posP, c2P);
      if (pos2P == NUL) return *this;
      if (pos1P == NUL || pos2P < pos1P)
      {
         posP = pos2P + 1;
         ctr--;
      }
      else
      {
         posP = pos1P + 1;
         ctr++;
      }
   }

   pos1P = strstr(chaineP, c1P) + strlen(c1P);
   pos2P = posP - 1;
   strncpy(chaineP, pos1P, pos2P-pos1P);
   chaineP[pos2P-pos1P] = 0x0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::bloc

QBString bloc(const QBString& copie, const QBString& s1, const QBString& s2)
{
   QBString res = copie;
   return res.bloc(s1, s2);
}  // QBString::bloc

QBString bloc(const QBString& copie, ConstCarP c1P, ConstCarP c2P)
{
   QBString res = copie;
   return res.bloc(c1P, c2P);
}  // QBString::bloc


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString rbloc(const QBString& copie, const QBString& s1, const QBString& s2)
{
   QBString res = copie;
   return res.rbloc(s1, s2);
}  // QBString::rbloc

QBString rbloc(const QBString& copie, ConstCarP c1P, ConstCarP c2P)
{
   QBString res = copie;
   return res.rbloc(c1P, c2P);
}  // QBString::rbloc


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::subst(ConstCarP c1P, ConstCarP c2P)
{
#ifdef MODE_DEBUG
   PRECONDITION(c1P  != 0);
   PRECONDITION(*c1P != 0);
   PRECONDITION(c2P  != 0);
   PRECONDITION(*c2P != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   char *posP;

   posP = strstr(chaineP, c1P);
   if (posP == NUL)
      return *this;
   *posP = '\0';

   QBString tmp = chaineP;
   tmp += c2P;
   tmp += posP+strlen(c1P);
   (*this) = tmp;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::subst

QBString subst(const QBString& copie, const QBString& s1, const QBString& s2)
{
   QBString res = copie;
   return res.subst(s1, s2);
}  // QBString::subst

QBString subst(const QBString& copie, ConstCarP c1P, ConstCarP c2P)
{
   QBString res = copie;
   return res.subst(c1P, c2P);
}  // QBString::subst


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::token(ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   PRECONDITION(*cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   if (strstr(chaineP, cP) == NUL)
   {
      memset(chaineP, 0x0, MAXSIZE-1);
      return *this;
   }

   Booleen done = FAUX;
   char*   pos1P = chaineP;
   while (! done)
   {
      Entier  ctrPar = 0, ctrAcc = 0, ctrCro = 0;
      Booleen singleQuote = FAUX, doubleQuote = FAUX;
      char* pos2P = strstr(pos1P, cP);
      if (pos2P == NUL)
      {
         memset(chaineP, 0x0, MAXSIZE-1);
         return *this;
      }
      char prec = ' ';
      for (char *posP = chaineP; posP < pos2P; posP++)
      {
         switch (*posP)
         {
            case '\'' : if (!doubleQuote) singleQuote = !singleQuote; break;
            case '\"' : if (!singleQuote) doubleQuote = !doubleQuote; break;
            case '('  : if (!singleQuote && !doubleQuote) ctrPar++; break;
            case ')'  : if (!singleQuote && !doubleQuote) ctrPar--; break;
            case '{'  : if (!singleQuote && !doubleQuote) ctrAcc++; break;
            case '}'  : if (!singleQuote && !doubleQuote) ctrAcc--; break;
            case '<'  : if (!singleQuote && !doubleQuote && *(posP+1) != '=') ctrCro++; break;
            case '>'  : if (!singleQuote && !doubleQuote && *(posP+1) != '=' && prec != '-') ctrCro--; break;
         }
         prec = *posP;
      }
      pos1P = pos2P+1;
      if (ctrPar == 0 && ctrAcc == 0 && ctrCro == 0)
         done = VRAI;
   }

   left( static_cast<Entier>(pos1P-chaineP-1) );

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::token

QBString token(const QBString& copie, const QBString& s)
{
   QBString res = copie;
   return res.token(s);
}  // QBString::token

QBString token(const QBString& copie, ConstCarP cP)
{
   QBString res = copie;
   return res.token(cP);
}  // QBString::token


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString rtoken(const QBString& copie, const QBString& s1)
{
   QBString res = copie;
   return res.rtoken(s1);
}  // QBString::rtoken

QBString rtoken(const QBString& copie, ConstCarP c1P)
{
   QBString res = copie;
   return res.rtoken(c1P);
}  // QBString::rtoken


//**************************************************************
// Sommaire:
//
// Description:
//    Supprime les espaces au début de la chaîne
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::ltrim()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   CarP cP = chaineP;
   while (*cP == ' ') cP++;
   if (cP > chaineP)
      strcpy(chaineP, cP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::ltrim

QBString ltrim(const QBString& copie)
{
   QBString res(copie);
   return res.ltrim();
}  // QBString::ltrim


//**************************************************************
// Sommaire:
//
// Description:
//    Supprime les espaces aux deux extrémités de la chaîne
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::trim()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   CarP cP;
   cP = chaineP;
   while (*cP == ' ') cP++;
   if (cP > chaineP)
      strcpy(chaineP, cP);

   if (*chaineP != 0x0)
   {
      cP = chaineP + strlen(chaineP) - 1;
      while (*cP == ' ' && cP > chaineP) *cP-- = 0x0;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::trim

QBString trim(const QBString& copie)
{
   QBString res(copie);
   return res.trim();
}  // QBString::trim


//**************************************************************
// Sommaire:
//
// Description:
//    Supprime les espaces à la fin de la chaîne
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
const QBString& QBString::rtrim()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   if (*chaineP != 0x0)
   {
      CarP cP = chaineP + strlen(chaineP) - 1;
      while (*cP == ' ' && cP > chaineP) *cP-- = 0x0;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::rtrim

QBString rtrim(const QBString& copie)
{
   QBString res(copie);
   return res.rtrim();
}  // QBString::rtrim


//**************************************************************
// Sommaire:
//
// Description:
//    Surcharge de l'opérateur + pour une QBString, une chaîne C
//    et un char.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
QBString QBString::operator + (const QBString& copie) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   QBString res(*this);
   res += copie.chaineP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return res;
}  // QBString::operator +

QBString QBString::operator + (ConstCarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   QBString res(*this);
   res += cP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return res;
}  // QBString::operator +

QBString QBString::operator + (Car c) const
{
#ifdef MODE_DEBUG
   PRECONDITION(c != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   QBString res = (*this);
   res += c;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return res;
}  // QBString::operator +

QBString operator + (ConstCarP cP, const QBString& copie)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
#endif   // ifdef MODE_DEBUG

   QBString res(cP);
   res += copie.chaineP;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return res;
}  // QBString::operator +

QBString operator + (Car c, const QBString& copie)
{
#ifdef MODE_DEBUG
   PRECONDITION(c != 0);
#endif   // ifdef MODE_DEBUG

   QBString res = " " + copie;
   res.chaineP[0] = c;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return res;
}  // QBString::operator +


