//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:
// Classe:
//*****************************************************************************
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//*****************************************************************************
#include "prfictmp.h"

#if defined(__WIN32__)
#  define ACCESS _access
#else
#  include <unistd.h>
#  define ACCESS access
#endif
#include <cstdio>
#include <cstdlib>

//*****************************************************************************
// Sommaire:     Constructeur par défaut
//
// Description:
//    Le constructeur par défaut <code>PRNomFichierTemporaire()</code> génère
//    un nom de fichier unique. Il tente en premier de le créer dans le
//    répertoire temporaire, identifié par les variables d'environnement TEMP,
//    TMP ou encore temp.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
PRNomFichierTemporaire::PRNomFichierTemporaire()
{
   // ---  Forme le nom du repertoire temporaire
   PRStatement tmpDir = std::getenv("TEMP");
   if (tmpDir == "")
      tmpDir = std::getenv("TMP");
   if (tmpDir == "")
      tmpDir = std::getenv("tmp");
   if (tmpDir == "")
   {
//      tmpDir = left(_argv[0], rinstr(_argv[0], \\));
   }

   Booleen trouve = FAUX;
   if (tmpDir != "")
   {
      char buf[32];
      const int IMAX = 1679616;   // (36^4)
      for (int i = 0; i < IMAX; ++i)
      {
         std::sprintf(buf, "\\~Web++%x.tmp", i);
         nom = tmpDir + buf;
         if (ACCESS(nom, 0) != 0)
         {
            trouve = VRAI;
            break;
         }
      }
   }

   if (!trouve)
   {
      nom = std::tmpnam(0);
   }
}

//*****************************************************************************
// Sommaire:   Destructeur de la classe.
//
// Description:
//    Le destructeur <code>~PRNomFichierTemporaire()</code> efface le fichier
//    temporaire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
PRNomFichierTemporaire::~PRNomFichierTemporaire()
{
   if (nom != "") std::remove(nom);
}

