setlocal
echo off

set SRC_DIR=%INRS_DEV%\H2D2
::set WPP_DIR="%ProgramFiles%\WebFtn"
::set DST_DIR=%INRS_DEV%\WebFtn
set WPP_DIR="%INRS_DEV%\Parser\TrfFtn\bin"
set DST_DIR=.

:: -----------------------------------------------------------
:: Monte la liste des fichiers � traiter
:: -----------------------------------------------------------
PUSHD %SRC_DIR%
dir /s /b *.fi   >  %TEMP%\webpp-lst.rsp
dir /s /b *.fc   >> %TEMP%\webpp-lst.rsp
dir /s /b *.for  >> %TEMP%\webpp-lst.rsp
POPD

:: -----------------------------------------------------------
:: Param�tres d'ex�cution de TrfFtn
:: -----------------------------------------------------------
set TRF_PRM=
set TRF_PRM=%TRF_PRM% -p=/H2D2

:: -----------------------------------------------------------
:: TrfFtn
:: -----------------------------------------------------------
PUSHD %DST_DIR%
%WPP_DIR%\TrfFtn %TRF_PRM% @%TEMP%\webpp-lst.rsp > %WPP_DIR%\TrfFtn.log 2>&1
POPD

:: -----------------------------------------------------------
:: Nettoie
:: -----------------------------------------------------------
if exist %TEMP%\webpp-lst.rsp del %TEMP%\webpp-lst.rsp

endlocal