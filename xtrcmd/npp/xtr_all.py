#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
         xtrDir = os.path.join(home, 'toolbox/xtrcmd')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrcmd
import PyUtil

class Recursion(PyUtil.H2D2Recursion):
    def xeqAction(self, fullPath, src, mdl):
        fic  = [ os.path.join(fullPath, s) for s in src ]
        inc = os.path.join(home, 'H2D2/h2d2.i')
        args = '@%s -f chm -k module=%s %s' % (inc, mdl, ' '.join(fic))
        args = args.split(' ')
        xtrcmd.main(args)

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if argv == None: argv = sys.argv[1:]
    if len(argv) == 0: argv.append('.')
    r = Recursion()
    for a in argv:
        r.xeqRecursionOnSConscript(a)

main()
