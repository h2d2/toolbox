#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import hashlib
import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
         xtrDir = os.path.join(home, 'toolbox/xtrcmd')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrcmd

def xeqRecursion(dir, mdl = None, mdlDir = None):
    if (os.path.isdir( os.path.join(dir, 'build') ) and
        os.path.isdir( os.path.join(dir, 'prjVisual') ) and
        os.path.isdir( os.path.join(dir, 'source') )):
        mdlDir = dir
        mdl = os.path.basename(dir)
        mdl = os.path.splitext(mdl)[0]

    if os.path.isdir(dir):
        for f in os.listdir(dir):
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath) and f != 'proto':
                xeqRecursion(fullPath, mdl, mdlDir)
            elif (os.path.splitext(fullPath)[1].lower() == '.for'):
                if (mdl and mdlDir): xeqAction(fullPath, mdl, mdlDir)
    else:
        if (mdl and mdlDir): xeqAction(dir, mdl, mdlDir)

def xeqAction(dir, mdl, mdlDir):
    inp = dir

    out_d = os.path.dirname(dir)                # Cut file name
    out_d = mdlDir                              #
    out_f = os.path.basename(dir)               # File name
    out_f = os.path.splitext(out_f)[0]          # Cut extension
    out_f = '.'.join( [out_f, 'tree', 'xml'] )  # Add new extension
    out = os.path.join(out_d, 'doc', out_f)     # Full output path
    bck = '.'.join( [out, 'bak'] )              # Backup

    inc = os.path.join(home, 'H2D2/h2d2.i')
    tmp = out + '.new'
    args = '@%s -f tree -k module=%s -o %s %s' % (inc, mdl, tmp, inp)
    args = args.split(' ')
    try:
        xtrcmd.main(args)
    except:
        pass

    if (os.path.isfile(tmp)):
        if (os.path.isfile(out)):
            tmp_fic = open(tmp, 'rb')
            out_fic = open(out, 'rb')
            tmp_md5 = hashlib.md5( tmp_fic.read() )
            out_md5 = hashlib.md5( out_fic.read() )
            tmp_fic.close()
            out_fic.close()
            if (tmp_md5.digest() != out_md5.digest()):
                if (os.path.isfile(bck)): os.remove(bck)
                os.renames(out, bck)
                os.renames(tmp, out)
                logger.info(' --> Updating %s' % out)
            else:
                os.remove(tmp)
        else:
            os.renames(tmp, out)
            logger.info(' --> Creating %s' % out)

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if (argv == None): argv = sys.argv[1:]
    if (len(argv) == 0): argv.append('.')
    for a in argv:
        xeqRecursion(a)

#main( [ os.path.join(os.environ['INRS_DEV'], 'H2D2/h2d2_algo/source/alnobi_ic.for') ] )
main()
