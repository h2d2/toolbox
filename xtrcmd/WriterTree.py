#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import H2D2Commands

class Writer:
    indents = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'

    def __init__(self, fname):
        self.indent = 0
        self.fnam = fname
        self.fout = None

    def __del__(self):
        if (self.fout): self.fout.close()

    def __openFile(self, n):
        if (not self.fnam or self.fnam == ""):
            if (self.fout): self.fout.close()
            self.fout = open(n + '.tree.xml', 'w')
        else:
            if (not self.fout): self.fout = open(self.fnam, 'w', encoding='utf-8')

    def __closeFile(self):
        if self.fout: self.fout.close()
        self.fout = None

    def __writeLine(self, l):
        if (self.indent > 0):
            ll = '%s%s\n' % (Writer.indents[:self.indent], l)
        else:
            ll = '%s\n' % (l)
        self.fout.write(ll)

    def __writeEntry(self, start, end, **kwargs):
        tags = []
        for k, v in kwargs.items():
            if (v and v.strip() != ''): tags.append('%s="%s"' % (k, v.strip()))
        self.__writeLine('%s %s %s' % (start, ' '.join(tags), end))

    def __writeOneFnc(self, f):
        self.__writeEntry('<entry', '/>', type='function', name=f.name, file=f.file)

    def __writeOneMth(self, f):
        self.__writeEntry('<entry', '/>', type='method', name=f.name, file=f.file)

    def __writeOneCls(self, cls):
        self.__writeEntry('<tree', '>', type='class', name=cls.name, file=cls.file)
        self.indent += 1
        for m in cls.meths: self.__writeOneMth(m)
        self.indent -= 1
        self.__writeLine('</tree>')

    def __writeOneMdl(self, mdl):
        self.__writeEntry('<tree', '>', type='module', name=mdl.name, file=mdl.file)
        self.indent += 1
        for c in mdl.cmps:
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeOneMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeOneCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeOneFnc(c)
        self.indent -= 1
        self.__writeLine('</tree>')

    def __writeOneCmd(self, mth):
        self.__writeLine('<tree type="command" name="%s">' % (mth.name))
        self.__writeLine('</tree>')

    def __writeMdl(self, mdl):
        self.__openFile(mdl.name)
        self.__writeOneMdl(mdl)
        self.__closeFile()

    def __writeCls(self, cls):
        self.__openFile(cls.name)
        self.__writeOneCls(cls)
        self.__closeFile()

    def __writeCmd(self, mdl):
        self.__openFile(mdl.name)
        self.__writeOneCmd(mdl)
        self.__closeFile()

    def __scanForTags(self, itm, tags):
        if (isinstance(itm, H2D2Commands.Mdl)): tags.append(itm.name)
        if (isinstance(itm, H2D2Commands.Cls)): tags.append(itm.name)
        try:
            for c in itm.cmps: self.__scanForTags(c, tags)
        except:
            pass
        try:
            for c in itm.meths: self.__scanForTags(c, tags)
        except:
            pass

    def write(self, cmds, **kwargs):
        self.cmds = cmds

        for c in cmds:
            self.cmdActu = c
            self.tags = []
            self.__scanForTags(c, self.tags)
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeCmd(c)
