#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import H2D2Commands
import WriterHtml

class Writer(WriterHtml.Writer):

    def __init__(self, fname):
        print(fname)
        WriterHtml.Writer.__init__(self, fname)

    def __writeHdr(self, titleGeneric, titleSpecific, tags):
        hdr = \
'''\
<!-- ************************************************************************ -->
<!--  h2d2 xtrcmd 17.10 -->
<!--  Copyright (c) INRS 2006-2017 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="%s">
<link rel="stylesheet" type="text/css" href="h2d2_cmd.css">
<title>%s</title>
</head>
<body>
''' % (', '.join(tags), titleSpecific)
        self.__writeLine( hdr )
        if (self.module != ''):
            self.__writeLine('')
            self.__writeLine( '<!--Entry type="module" name="%s" -->' % (self.module))
