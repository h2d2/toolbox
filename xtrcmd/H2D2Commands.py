#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Classes et algorithmes pour extraire des fichiers PyFtn
les commandes avec les commentaires.
"""

import PyFtn
import os
import re

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran.xtrcmd")

class gpi:
    """
    Global Positionning Index
    """
    cnt = 0
    @staticmethod
    def next():
        gpi.cnt += 1
        return gpi.cnt

class Arg:
    """
    Un argument
    """
    def __init__(self, pos, name, type):
        self.position = pos
        self.name = name
        self.type = type
        self.desc = ''

class Component:
    def __init__(self, n, d = '', s = ''):
        logger.debug('Component.__init__(%s)' % n)
        self.file  = ''
        self.scop  = s
        self.name  = n
        self.desc  = d
        self.pos   = gpi.next()

    def setFile(self, f):
        self.file = f

    def setScop(self, f):
        self.scop = f

    def setDesc(self, f):
        self.desc = f

class Fnc(Component):
    """
    Une fonction, méthode
    """
    def __init__(self, n, d = '', type = '##__unset__##', stype = [], args = []):
        logger.debug('Fnc.__init__(%s)' % n)
        Component.__init__(self, n, d)
        self.type  = type    # fonction, property, op
        self.stype = stype   # get, set
        self.args  = args
        self.rtrn  = None

    def setArgs(self, cod):
        self.args  = cod.args
        self.rtrn  = cod.rtrn

class Cls(Component):
    """
    Une classe
    """
    def __init__(self, n, d):
        logger.debug('Cls.__init__(%s)' % n)
        Component.__init__(self, n, d)
        self.ctrs  = None
        self.meths = []
        self.sttcs = []

    def setConstructor(self, ctr):
        self.ctrs = ctr
        for c in self.ctrs:
            c.name = self.name
            c.scop = self

    def setMethods(self, mth):
        self.meths = mth
        for m in self.meths:
            if (m.type == 'function'): m.type = 'method'
            m.scop = self

    def setStaticMethods(self, mth):
        self.sttcs = mth
        for m in self.sttcs:
            if (m.type == 'function'): m.type = 'static method'
            if (m.type == 'method'):   m.type = 'static method'
            m.scop = self

class Mdl(Component):
    """
    Un module
    """
    def __init__(self, n, d):
        logger.debug('Mdl.__init__(%s)' % n)
        Component.__init__(self, n, d)
        self.cmps  = []

    def addComponent (self, cmp):
        self.cmps.append(cmp)
        self.cmps[-1].scop = self
        if (isinstance(self.cmps[-1], Fnc)):
            if (self.cmps[-1].type == 'method'):
                self.cmps[-1].type = 'function'

    def setComponents(self, cmps):
        self.cmps = cmps
        for m in self.cmps:
            m.scop = self
            if (isinstance(m, Fnc)):
                if (m.type == 'method'):
                    m.type = 'function'

class File(PyFtn.File):
    def __init__(self, nomFic, usrFncs = []):
        PyFtn.File.__init__(self, nomFic)
        self.usrFncs = usrFncs

    def isReqNom(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_REQNOM')
        return r

    def isXeqOpb(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_OPBDOT')
        return r

    def isXeqCtr(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_XEQCTR')
        return r

    def isReqMdl(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_REQMDL')
        return r

    def isReqCls(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_REQCLS')
        return r

    def isXeqMth(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_XEQMTH')
        return r

    def isXeqCmd(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-4:] == '_CMD')
        return r

    def isReqCmd(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_REQCMD')
        return r

    def isReqHlp(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName()[-7:] == '_REQHLP')
        return r

    def isUsrFnc(self):
        r = False
        if self.isFunction():
            f = PyFtn.Function(self, doParseCode = False)
            r = (f.getName() in self.usrFncs)
        return r

    def parse(self):
        cmds = []

        self.doStripCmt = True
        self.doInclude = True
        mdl, cmp, cls, ctr, mth, cmd, cod, hlp, usr = (None,)*9
        try:
            hdr = None
            while self:
                s1 = next(self)
                #print('... %s' % s1)
                #logger.debug('... %s' % s1)

                if s1.isFunction():
                    # ---  Module
                    if   s1.isReqMdl():
                        print('File.parse: Module: ', s1)
                        mdl = reqMdl(self, hdr)
                    elif s1.isReqNom():
                        print('File.parse: Module: ', s1)
                        mdl = reqNom(self, hdr)
                    elif s1.isXeqOpb():
                        print('File.parse: Module: ', s1)
                        cmp = xeqOpb(self, hdr)
                    # ---  Classe
                    elif s1.isReqCls():
                        print('File.parse: Class: ', s1)
                        cls = reqCls(self, hdr)
                    elif s1.isXeqCtr():
                        print('File.parse: Class: ', s1)
                        ctr = xeqCtr(self, hdr)
                    elif s1.isXeqMth():
                        print('File.parse: Class: ', s1)
                        mth = xeqMth(self, hdr)
                    # ---  Commande
                    elif s1.isReqCmd():
                        print('File.parse: Command: ', s1)
                        cmd = reqCmd(self, hdr)
                    elif s1.isXeqCmd():
                        print('File.parse: Command: ', s1)
                        cod = xeqCmd(self, hdr)
                    # ---  Help
                    elif s1.isReqHlp():
                        hlp = reqHlp(self, hdr)
                    # ---  User function
                    elif s1.isUsrFnc():
                        logger.debug('')
                        logger.debug('')
                        logger.debug('')
                        logger.debug('User function detected')
                        usr = XeqCtr(self, hdr)
                        logger.debug('User function end')
                        logger.debug('')
                        logger.debug('')
                        logger.debug('')
                elif s1.isHeader():
                    hdr = PyFtn.Header(self)
                    if (hdr.empty): hdr = None

        except StopIteration:
            pass
        except UnicodeDecodeError:
            logger.info('Error: non UTF-8 file; skipping %s' % self.getFileName())
            return []

        if mdl and cmp:
            mdl.setComponents(cmp)
            mdl.setFile(self.getFileName())
            cmds.append(mdl)
        if cls and ctr:
            cls.setConstructor(ctr)
            if mth: cls.setMethods(mth)
            if cmp: cls.setStaticMethods(cmp)
            cls.setFile(self.getFileName())
            clsName = cls.name
            if (clsName[0:8] == '__dummy_'):
                logger.debug('   Skipping %s' % clsName)
            else:
                cmds.append(cls)
        if (cmd and cod):
            cmd.setArgs(cod[0])
            cmd.setFile(self.getFileName())
            cmds.append(cmd)
        if (hlp):
            cmds.append(hlp)
        if (usr):
            cmds.append(usr)

        return cmds

class SP_STRN_TKx:
    def __init__(self):
        pass

    def parse(self, sta):
        #logger.debug('SP_STRN_TKx.parse(%s)' % str(sta))
        type = None
        l = sta.getAsTxt()
        l = l[l.index('SP_STRN_TK'):]
        if (l[0:11] =='SP_STRN_TKI'): type = 'integer'
        if (l[0:11] =='SP_STRN_TKR'): type = 'double'
        if (l[0:11] =='SP_STRN_TKS'): type = 'string'
        l = l[11:]
        l = l.strip()
        if (l[0] != '('): raise ValueError(l)

        l = PyFtn.String(l).getBlock('(', ')')
        l = l.strip()

        args = l.split(',')
        src = args[0].strip()
        pos = args[-2].strip()
        var = args[-1].strip().lower()

        # ---  Change le type des handle
        if (var[0] == 'h'): type = 'handle'
        if (var[0] == 'H'): type = 'handle'

        self.src = src
        self.pos = pos
        self.var = var
        self.type = type

    def getSource(self):
        return self.src

    def getPosition(self):
        return self.pos

    def getVariable(self):
        return self.var

    def getType(self):
        return self.type


class Cmt:
    def __init__(self):
        self.cmt = []

    def parse(self, sta):
        if isinstance(sta, PyFtn.FtnIf): return
        if sta.getAsTxt()[0:9] != '<comment>': return
        #logger.debug('Cmt.parseCmt(%s)' % str(sta))

        tag = PyFtn.TagString(sta)
        tag = tag.getTxt()
        if tag[ :6] == '<crlf>': tag = tag[6:]
        if tag[-6:] == '<crlf>': tag = tag[:-6]
        self.cmt = tag.strip().split('<crlf>')

    def getComment(self):
        return self.cmt

class Parser_opb_par(PyFtn.Function):
    """
    Algorithme pour parser un appel de fonction ou constructeur.
    En fin de parse, contient un objet Fnc.
    """

    def __init__(self, ftnSta, hdr = None):
        logger.debug('Parser_opb_par.__init__(%s)' % str(ftnSta)[0:50])

        self.fcs = []
        self.fnc  = Fnc('##opb_par_dummy_name##', type = '##opb_par_dummy_type', args =[])
        self.cmt  = Cmt()   # Var de travail pour parseStatement
        PyFtn.Function.__init__(self, ftnSta, hdr)

    """
    Spécialisation de parseStatement du parent
    """
    def parseStatement(self, s):
        logger.debug('Parser_opb_par.parseStatement(%s)', str(s))
        l = s.getAsTxt()
        l = l.strip()
        lc = l.replace(' ', '')

        if lc.find('WRITE(IPRM,') >= 0:
            arg = self.__filterRtrn(l)
            if (arg):
                arg.desc = self.cmt.getComment()
                self.fnc.rtrn = arg
                self.cmt  = Cmt()
        elif l.find('IPRM') >= 0:
            arg = self.__filterArg(l)
            if (arg):
                arg.desc = self.cmt.getComment()
                self.fnc.args.append(arg)
                self.cmt  = Cmt()
        elif s.isComment():
            pass
        elif s.isTagComment():
            self.cmt.parse(s)
            self.fnc.desc = self.cmt.getComment()

    """
    Spécialisation de parseFunctionEnd du parent
    """
    def parseFunctionEnd(self):
        logger.debug('Parser_opb_par.parseFunctionEnd(%s)' % self)
        if (self.fnc.rtrn):
            self.fcs.append(self.fnc)
            self.fnc  = Fnc('##opb_par_dummy_name##', type = '##opb_par_dummy_type', args =[])

    def __filterArg(self, l):
        logger.debug('Parser_opb_par.__filterArg(%s)' % l)
        ret = None
        if (len(l) == 0): return ret
        if (l[0:2] == '99'): return ret     # Labels d'erreur

        lc = l.replace(' ', '')
        if (lc[0:10] == 'CHARACTER*'): return ret
        if (lc[0:26] == 'IF(SP_STRN_LEN(IPRM).LE.0)'): return ret
        if (lc[0: 6] == 'WRITE('): return ret

        if (lc[0:13] == 'IF(IERR.EQ.0)' or lc[0:13] == 'IF(IRET.EQ.0)'):
            l = l[ l.index(')')+1 :]
            l = l.lstrip()
        lc = l.replace(' ', '')
        if (lc[0:15] == 'IERR=SP_STRN_TK' or lc[0:15] == 'IRET=SP_STRN_TK'):
            tk = SP_STRN_TKx()
            tk.parse( PyFtn.Statement(l) )
            if (tk.getSource() != 'IPRM'): raise ValueError(l)
            ret = Arg(tk.getPosition, tk.getVariable(), tk.getType())

        return ret

    def __filterRtrn(self, l):
        logger.debug('Parser_opb_par.__filterRtrn(%s)' % l)
        ret = None
        if (len(l) == 0): return ret
        if (l[0:2] == '99'): return ret     # Labels d'erreur

        l = l[l.find(")')")+3:]
        l = l.lstrip()
        tp = l[:l.find(',')].strip()
        tk = l[l.rfind(',')+1:].strip()
        if (len(tp) == 0): return ret
        if (tp[0] != '\''): return ret
        if (tp[-1] != '\''): return ret
        tp = tp[1:-1]
        if (len(tk) == 0): return ret
        type = None
        if (tp == 'S'): type = 'string'
        if (tp == 'I'): type = 'integer'
        if (tp == 'R'): type = 'double'
        if (tp == 'H'): type = 'handle'
        ret = Arg(0, tk, type)

        return ret

    def getMethods(self):
        return self.fcs

class Parser_opb_dot(PyFtn.Function):

    def __init__(self, ftnSta, hdr = None):
        self.comps = []
        self.glbls = {}
        self.mduls = {}
        self.cmt = Cmt()
        PyFtn.Function.__init__(self, ftnSta, hdr)

    """
    Spécialisation de parseStatement du parent
    """
    def parseStatement(self, s):
        #logger.debug('Parser_opb_dot.parseStatement(%s)' % str(s)[0:50])
        l = s.getAsTxt()
        l = l.strip()
        if s.isComment():
            pass
        elif s.isTagComment():
            logger.debug('Parser_opb_dot.parseStatement comment found: %s', s)
            self.cmt.parse( PyFtn.Statement(l) )
        elif s.isReqCls():
            logger.debug('Parser_opb_dot.parseStatement class found: %s', s)
            prs = Parser_nom(s)
            cls = Cls(prs.getName(), prs.getDesc())
            scp = "%s.%s" % ('__modul__', cls.name)
            self.__addComp(scp, cls)
            logger.debug('Parser_opb_dot.parseStatement class done')
        elif s.isXeqCtr():
            logger.debug('Parser_opb_dot.parseStatement ctr found: %s', s)
            mth = Parser_opb_par(s).getMethods()
            mth.type = 'constructor'
            logger.debug('Parser_opb_dot.parseStatement ctr done')
        elif s.isXeqMth():
            logger.debug('Parser_opb_dot.parseStatement Methods found: %s', s)
            m = Parser_opb_dot(s)
            try:
                self.comps.extend( m.comps )
            except:
                self.comps.append( m )
        elif s.isFunction():
            logger.debug('Parser_opb_dot.parseStatement function found: %s', s)
            self.parseCode(s)
        elif s.isSubroutine():
            logger.debug('Parser_opb_dot.parseStatement subroutine found: %s', s)
            self.parseCode(s)
        elif self.__isStart(l):
            logger.debug('Parser_opb_dot.parseStatement if(imeth.eq. found')
            if_ = PyFtn.FtnIf(l, s)
            self.__parseIf(if_, scope = "__modul__")
            logger.debug('Parser_opb_dot.parseStatement if(imeth.eq. end')
        return

    def __parseIf(self, if_, scope = ' '):
        logger.debug('Parser_opb_dot.__parseIf(%s) scope(%s)' % (str(if_)[0:80], scope))
        if (isinstance(if_, PyFtn.FtnIf)):
            for blk in if_:
                tst = blk[1]                # Test of the if block
                mth = self.__filterMthName (tst)
                prp = self.__filterPropName(tst)
                mdl = self.__filterMdulName(tst)
                cls = self.__filterClsName(tst)
                logger.debug('Parser_opb_dot.__parseIf: statement %s' % tst)
                if (mth == '##property_get##'):
                    logger.debug('Parser_opb_dot.__parseIf: detected %s' % mth)
                    scp = "%s.%s" % (scope, '__get__')
                elif (mth == '##property_set##'):
                    logger.debug('Parser_opb_dot.__parseIf: detected %s' % mth)
                    scp = "%s.%s" % (scope, '__set__')
                elif (mth == '##opb_[]_get##'):
                    logger.debug('Parser_opb_dot.__parseIf: detected %s' % mth)
                    scp = "%s.%s" % (scope, '__get__')
                    self.__parseOpAcc(blk[2], scp, 'get')
                elif (mth == '##opb_[]_set##'):
                    logger.debug('Parser_opb_dot.__parseIf: detected %s' % mth)
                    scp = "%s.%s" % (scope, '__set__')
                    self.__parseOpAcc(blk[2], scp, 'set')
                elif (len(mdl) > 0):
                    logger.debug('Parser_opb_dot.__parseIf: detected module %s.%s' % (scope, mdl))
                    scp = "%s.%s" % (scope, mdl)
                    self.__parseMdul(blk[2], scp, mdl)
                elif (len(cls) > 0):
                    logger.debug('Parser_opb_dot.__parseIf: detected class %s.%s' % (scope, mdl))
                    scp = "%s.%s" % (scope, cls)
                    self.__parseClss(blk[2], scp, cls)
                elif (len(mth) > 0):
                    logger.debug('Parser_opb_dot.__parseIf: detected method %s.%s' % (scope, mth))
                    scp = "%s.%s" % (scope, mth)
                    self.__parseMeth(blk[2], scp, mth)
                elif (len(prp) > 0):
                    logger.debug('Parser_opb_dot.__parseIf: detected property %s.%s' % (scope, prp))
                    scp = "%s.%s" % (scope, prp)
                    self.__parseProp(blk[2], scp, prp)
                else:
                    scp = scope

                for s in blk[2]:
                    self.__parseIf(s, scp)
        else:
            desc = self.cmt.parse(if_)
        #logger.debug('Parser_opb_dot.__parseMth(%s) done' % str(if_)[0:80])

    @staticmethod
    def cleanScope(scope):
        return '.'.join( [x for x in scope.split('.') if not (x[:2] == '__' and x[-2:] == '__')] )

    def __addComp(self, s, c):
        logger.debug('Parser_opb_dot.__addComp: add component %s to scope %s' % (c, s))
        if (isinstance(c, Mdl)):
            sc = Parser_opb_dot.cleanScope(s)
            self.mduls[sc] = c
            self.comps.append(c)
        else:
            try:
                sc = Parser_opb_dot.cleanScope(s)
                sc = '.'.join( sc.split('.')[:-1] )
                self.mduls[sc].addComponent(c)
            except:
                self.comps.append(c)
        self.glbls[s] = c

    def __parseMdul(self, blk, scope, mdl):
        logger.debug('Parser_opb_dot.__parseMdul(%s)' % mdl)

        name = Parser_opb_dot.cleanScope(scope)
        desc = self.cmt.getComment()
        self.cmt = Cmt()
        args, rtrn = self.__parseArgs(blk)
        m = Mdl(name, desc)
        self.__addComp(scope, m)

        logger.debug('Parser_opb_dot.__parseMdul(%s) done' % mdl)

    def __parseClss(self, blk, scope, cls):
        logger.info('Parser_opb_dot.__parseClss(%s)' % cls)

        p = Parser_nom(blk)
        name = Parser_opb_dot.cleanScope(scope)
        desc = p.getDesc()
        c = Cls(name, desc)
        self.__addComp(scope, c)

        logger.info('Parser_opb_dot.__parseClss(%s) done' % cls)

    def __parseMeth(self, blk, scope, mth):
        logger.debug('Parser_opb_dot.__parseMeth(%s)' % mth)

        name = Parser_opb_dot.cleanScope(scope)
        desc = self.cmt.getComment()
        self.cmt = Cmt()
        args, rtrn = self.__parseArgs(blk)
        f = Fnc(name, type = 'method', stype = [], args = args)
        f.desc = desc
        f.rtrn = rtrn
        self.__addComp(scope, f)

        logger.debug('Parser_opb_dot.__parseMeth(%s) done' % mth)

    def __parseProp(self, blk, scope, mth):
        logger.debug('Parser_opb_dot.__parseProp(%s)' % mth)

        scold = scope.split('.')
        if (scold[-2] == '__get__'):
            stype = 'get'
            scold[-2] = '__set__'
        else:
            stype = 'set'
            scold[-2] = '__get__'
        scold = '.'.join(scold)

        if scold in self.glbls:
            self.glbls[scold].type = 'property'
            self.glbls[scold].stype.append(stype)
        else:
            name = Parser_opb_dot.cleanScope(scope)
            if (stype == 'get'):
                f = Fnc(name, type = 'getter', stype = [stype], args=[])
            else:
                f = Fnc(name, type = 'setter', stype = [stype], args=[])
            f.desc = self.cmt.getComment()
            self.cmt = Cmt()
            self.__addComp(scope, f)
        logger.debug('Parser_opb_dot.__parseProp(%s) done' % mth)

    def __parseOpAcc(self, blk, scope, stype):
        logger.debug('Parser_opb_dot.__parseOpAcc(%s)' % str(blk))

        nam = '(%s) op[]' % stype
        if nam not in self.glbls:
            f = Fnc(nam, type = 'operator', args=[])
            f.desc = self.cmt.getComment()
            self.cmt = Cmt()
        else:
            desc = self.cmt.getComment()
            self.cmt = Cmt()
            args, rtrn = self.__parseArgs(blk)
            f = Fnc(nam, type = 'operator', stype = [], args = args)
            f.desc = desc
            f.rtrn = rtrn
        self.__addComp(nam, f)
        logger.debug('Parser_opb_dot.__parseOpAcc(%s) done' % str(blk))

    def __parseArgs(self, blk):
        #logger.debug('Parser_opb_dot.__parseArgs(...)')
        args = []
        rtrn = None
        for l in blk:
            if (isinstance(l, PyFtn.FtnIf)):
                for b in l:
                    a2_, r2_ = self.__parseArgs(b[2])
                    args.extend(a2_)
                    if (r2_): rtrn = r2_
            elif (l.isTagComment()):
                self.cmt.parse(l)
            else:
                lc = l.getAsTxt().replace(' ', '')
                if (lc[0:15] == 'IERR=SP_STRN_TK' or lc[0:15] == 'IRET=SP_STRN_TK'):
                    tk = SP_STRN_TKx()
                    tk.parse(l)
                    if (tk.getSource() != 'IPRM'): raise ValueError(l)
                    arg = Arg(tk.getPosition, tk.getVariable(), tk.getType())
                    arg.desc = self.cmt.getComment()
                    self.cmt = Cmt()
                    args.append(arg)
                elif (lc.find('WRITE(IPRM,') >= 0):
                    ret = self.__parseRtrn(lc)
                    if (ret):
                        ret.desc = self.cmt.getComment()
                        rtrn = ret
                        self.cmt  = Cmt()
                        self.desc = ''
        return args, rtrn

    def __parseRtrn(self, l):
        types = { 'S' : 'string', 'I' : 'integer', 'R' : 'double', 'H' : 'handle'}

        #logger.debug('Parser_opb_dot.__parseRtrn(%s)' % l)
        ret = None
        if (len(l) == 0): return ret
        if (l[0:2] == '99'): return ret     # Labels d'erreur

        #WRITE(IPRM, '(2A,I12)') 'I', ',', x
        xprFmt = r'(\'\(.*\)\')'                                        # Format '( .* )'
        xprWrt = r'(WRITE\s*\(\s*IPRM\s*,\s*' + xprFmt  + r'\s*\))'     # WRITE
        xprTyp = r'(\'(?P<typ>[SIRH])\')'                               # Type
        xprVar = r'(?P<var>\w*\s*(\(.*\))?)'                            # Var
        xprUne = xprWrt + r'\s*' + xprTyp + '\s*,' + '.*,\s*' + xprVar + '$'
        patt = re.compile(xprUne)
        m = patt.match(l)
        if (m):
            t = m.group('typ')
            v = m.group('var')
            ret = Arg(0, v, types[t])

        return ret

    def __parseCmnt(self, blk):
        #logger.debug('Parser_opb_dot.__parseCmnt(%s)' % str(blk)[0:80])
        cmt = Cmt()
        for l in blk:
            if (isinstance(l, PyFtn.FtnIf)):
                for b in l:
                    cif = self.__parseCmnt(b[2])
                    if (len(cif.getComment()) > 0): cmt = cif
            else:
                cmt.parse(l)
        return cmt

    def __filterMthName(self, tst):
        lc = tst.replace(' ', '')
        if (lc[0:8] != 'IMTH.EQ.'): return ''
        tst = tst[tst.index('.EQ.')+4:]
        mth = tst.lstrip()
        if (mth[0] in ['"', "'"]): mth = mth[1:-1]
        return mth

    def __filterClsName(self, tst):
        lc = tst.replace(' ', '')
        if (lc[0:8] != 'CLSS.EQ.'): return ''
        tst = tst[tst.index('.EQ.')+4:]
        nam = tst.lstrip()
        nam = nam[1:-1]
        return nam

    def __filterMdulName(self, tst):
        lc = tst.replace(' ', '')
        if (lc[0:8] != 'MDUL.EQ.'): return ''
        tst = tst[tst.index('.EQ.')+4:]
        nam = tst.lstrip()
        nam = nam[1:-1]
        return nam

    def __filterPropName(self, tst):
        lc = tst.replace(' ', '')
        if (lc[0:8] != 'PROP.EQ.'): return ''
        tst = tst[tst.index('.EQ.')+4:]
        nam = tst.lstrip()
        nam = nam[1:-1]
        return nam

    def __isStart(self, l):
        lc = l.replace(' ', '')
        if (lc[0:11] != 'IF(IMTH.EQ.'): return False
        return True

    def getComponents(self):
        return self.comps

class Parser_nom(PyFtn.Function):
    def __init__(self, ftnSta, hdr = None):
        logger.debug('Parser_nom.__init__(%s)' % str(ftnSta)[0:50])

        self.cmt  = Cmt()
        self.name = None
        PyFtn.Function.__init__(self, ftnSta, hdr)

    """
    Spécialisation de parseStatement du parent
    """
    def parseStatement(self, s):
        #logger.debug('Parser_nom.parseStatement(%s)' % str(s)[0:50])
        l = s.getAsTxt()
        l = l.strip()
        if (s.isTagComment()):
            self.cmt.parse( PyFtn.Statement(l) )
        elif (len(l) > len(self.name) and l[0:len(self.name)] == self.name):
            l = l[len(self.name):]
            l = l.lstrip()
            l = l[1:]
            l = l.lstrip()
            self.name = l[1:-1]

    def getName(self):
        return self.name

    def getDesc(self):
        return self.cmt.getComment()

"""
================================================================================
    Module identifiée par les trois fonctions
    3)    xxx_REQMTH
    1)    xxx_OPBDOT
    2)    xxx_REQNOM
================================================================================
"""
def reqMdl(ftnSta, hdr = None):
    logger.debug('reqMdl.__init__(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isReqMdl()):
        raise ArgumentError('Not a _REQMDL function: %s', ftnSta)

    mth = Parser_opb_par(ftnSta, hdr).getMethods()
    for m in mth: m.typ = 'modul'
    return mth

def xeqOpb(ftnSta, hdr = None):
    logger.debug('xeqOpb(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isXeqOpb()):
        raise ArgumentError('Not a _OPBDOT function %s', ftnSta)

    cmps = Parser_opb_dot(ftnSta, hdr).getComponents()
    for m in mth: m.typ = 'static'
    return cmps

def reqNom(ftnSta, hdr = None):
    logger.debug('reqNom(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isReqNom()):
        raise ArgumentError('Not a _REQNOM function: %s', ftnSta)

    p = Parser_nom(ftnSta, hdr)
    return Mdl(p.getName(), p.getDesc())

"""
================================================================================
    Classe identifiée par les quatre fonctions
    1)    xxx_XEQCTR
    2)    xxx_XEQMTH
    3)    xxx_XEQOPB
    4)    xxx_REQCLS
================================================================================
"""
def xeqCtr(ftnSta, hdr = None):
    logger.debug('xeqCtr(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isXeqCtr()):
        raise ArgumentError('Not a _XEQCTR function %s', ftnSta)

    mths = Parser_opb_par(ftnSta, hdr).getMethods()
    for m in mths: m.type = 'constructor'
    return mths

def xeqMth(ftnSta, hdr = None):
    logger.debug('xeqMth(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isXeqMth()):
        raise ArgumentError('Not a _XEQMTH function %s', ftnSta)

    mths = Parser_opb_dot(ftnSta, hdr).getComponents()
    return mths

def xeqOpb(ftnSta, hdr = None):
    logger.debug('xeqOpb(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isXeqOpb()):
        raise ArgumentError('Not a _OPBDOT function %s', ftnSta)

    cmps = Parser_opb_dot(ftnSta, hdr).getComponents()
    return cmps

def reqCls(ftnSta, hdr = None):
    logger.debug('ReqCls.__init__(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isReqCls()):
        raise ArgumentError('Not a _REQCLS function: %s', ftnSta)

    p = Parser_nom(ftnSta, hdr)
    return Cls(p.getName(), p.getDesc())

"""
================================================================================
    Commande identifiée par les deux fonctions
    1)    xxx_CMD
    2)    xxx_REQCMD
================================================================================
"""
def xeqCmd(ftnSta, hdr = None):
    logger.debug('xeqCmd(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isXeqCmd()):
        raise ArgumentError('Not a _CMD function %s', ftnSta)

    mth = Parser_opb_par(ftnSta, hdr).getMethods()
    return mth

def reqCmd(ftnSta, hdr = None):
    logger.debug('reqCmd(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isReqCmd()):
        raise ArgumentError('Not a _REQCMD function %s', ftnSta)

    p = Parser_nom(ftnSta, hdr)
    return Fnc(p.getName(), d = p.getDesc(), type = 'function')

"""
================================================================================
    Help identifiée par les deux fonctions
    1)    xxx_REQHLP
================================================================================
"""
def reqHlp(ftnSta, hdr = None):
    logger.debug('ReqHlp.__init__(%s)' % str(ftnSta)[0:50])
    if (not ftnSta.isReqHlp()):
        raise ArgumentError('Not a _HLP function %s', ftnSta)

    p = Parser_nom(ftnSta, hdr)
    return Fnc(p.getName(), p.getDesc())
