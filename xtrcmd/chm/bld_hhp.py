#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait les commandes de H2D2
"""

import enum
import glob

Types = enum.Enum('Types', ('module', 'classe', 'command', 'method'))

class Info:
    def __init__(self, fic, typ, name, parent):
        if (typ == 'module'):  type = Types.module
        if (typ == 'class'):   type = Types.classe
        if (typ == 'command'): type = Types.command
        if (typ == 'method'):  type = Types.method
        self.file = fic
        self.type = type
        self.name = name
        self.prnt = parent

class Infos:
    def __init__(self):
        self.infos = []

    def __iter__(self):
        return self.infos.__iter__()

    def __next__(self):
        return self.infos.__next__()

    def __xtrTags(self, l):
        type = None
        name = None
        for t in l.split(' ', 1):
            var, val = t.split('=')
            var = var.strip()
            if (var == 'type'): type = val.strip()[1:-1]
            if (var == 'name'): name = val.strip()[1:-1]
        return type, name

    def readInfos(self, f):
        stk = [None]
        for l in open(f, 'r'):
            l = l.strip()
            if (l[0:10] == '<!--Entry '):
                l = l[10:-3].strip()
                type, name = self.__xtrTags(l)
                info = Info(f, type, name, stk[-1])
                self.infos.append(info)
                if (type == 'module'): stk.append(info)
                if (type == 'class'):  stk.append(info)
            elif (l[0:8] == '<!--End '):
                l = l[8:-3].strip()
                type, name = self.__xtrTags(l)
                if (type == 'module'): stk.pop()
                if (type == 'class'):  stk.pop()

    def tidyInfos(self):
        for i1 in self.infos:
            for i2 in self.infos:
                p2 = i2.prnt
                if p2 and p2.name == i1.name and p2.type == i1.type: i2.prnt = i1

    def getInfosWithParent(self, prnt):
        dico = {}
        for info in self.infos:
            if info.prnt is prnt:
                dico[info.name] = info
        return dico


class Writer:
    def __init__(self, fname):
        self.fout = open(fname, 'w')

    def __writeLine(self, l):
        ll = '%s\n' % (l)
        self.fout.write( ll )

    def __writeHdr(self):
        hdr = \
'''\
[OPTIONS]
Compatibility=1.1 or later
Compiled file=h2d2.chm
Contents file=h2d2_chm_content.hhc
Default topic=adaptative_relaxation.html
Display compile progress=No
Full-text search=Yes
Index file=h2d2_chm_index.hhk
Language=0xc0c Français (Canada)
Title=H2D2 17.10

[INFOTYPES]
TypeExclusive:command
TypeDesc:
TypeExclusive:class
TypeDesc:
TypeExclusive:module
TypeDesc:
TypeExclusive:method
TypeDesc:
'''
        self.__writeLine(hdr)

    def __writeFtr(self):
        self.__writeLine('')

    def __writeMenu(self, infos):
        dico = {}
        for info in infos:
            dico[info.file] = None
        keys = list(dico.keys())
        keys.sort()

        self.__writeLine('')
        self.__writeLine('[FILES]')
        for k in keys:
            self.__writeLine(k)

    def write(self, infos):
        self.__writeHdr()
        self.__writeMenu(infos)
        self.__writeFtr()

def main():
    infos = Infos()
    for f in glob.glob('*.html'):
        infos.readInfos(f)
    infos.tidyInfos()
    w = Writer("h2d2_chm.hhp")
    w.write(infos)

main()