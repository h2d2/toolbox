#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait les commandes de H2D2
"""

import enum
import glob

Types = enum.Enum('Types', ('module', 'classe', 'command', 'method'))

class Info:
    def __init__(self, fic, typ, name, parent):
        if (typ == 'module'):  type = Types.module
        if (typ == 'class'):   type = Types.classe
        if (typ == 'command'): type = Types.command
        if (typ == 'method'):  type = Types.method
        self.file = fic
        self.type = type
        self.name = name
        self.prnt = parent

class Infos:
    def __init__(self):
        self.infos = []

    def __iter__(self):
        return self.infos.__iter__()

    def __next__(self):
        return self.infos.__next__()

    def __xtrTags(self, l):
        type = None
        name = None
        for t in l.split(' ', 1):
            var, val = t.split('=')
            var = var.strip()
            if (var == 'type'): type = val.strip()[1:-1]
            if (var == 'name'): name = val.strip()[1:-1]
        return type, name

    def readInfos(self, f):
        stk = [None]
        for l in open(f, 'r'):
            l = l.strip()
            if (l[0:10] == '<!--Entry '):
                l = l[10:-3].strip()
                type, name = self.__xtrTags(l)
                info = Info(f, type, name, stk[-1])
                self.infos.append(info)
                if (type == 'module'): stk.append(info)
                if (type == 'class'):  stk.append(info)
            elif (l[0:8] == '<!--End '):
                l = l[8:-3].strip()
                type, name = self.__xtrTags(l)
                if (type == 'module'): stk.pop()
                if (type == 'class'):  stk.pop()

    def tidyInfos(self):
        for i1 in self.infos:
            for i2 in self.infos:
                p2 = i2.prnt
                if p2 and p2.name == i1.name and p2.type == i1.type: i2.prnt = i1

    def getInfosWithParent(self, prnt):
        dico = {}
        for info in self.infos:
            if info.prnt is prnt:
                dico[info.name] = info
        return dico

import os

class Writer:
    def __init__(self, fname):
        self.fout = open(fname, 'w', encoding='utf-8')

    def __writeLine(self, l):
        self.fout.write('%s\n' % l)

    def __writeHdr(self):
        hdr = \
'''\
<!-- ************************************************************************ -->
<!--  h2d2 xtrcmd 16.10 -->
<!--  Copyright (c) INRS 2006-2013 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>%s</title>
</head>
<body>
<OBJECT type="text/site properties">
	<param name="TypeExclusive" value="command">
	<param name="TypeExclusive" value="class">
	<param name="TypeExclusive" value="module">
	<param name="TypeExclusive" value="method">
</OBJECT>
''' % 'H2D2 - Commands'
        self.__writeLine(hdr)

    def __writeFtr(self):
        ftr = \
'''
</body>
</html>
'''
        self.__writeLine(ftr)

    def __writeIndex(self, dico):
        keys = list(dico.keys())
        keys.sort()
        for key in keys:
            self.__writeLine( '\t<li> <OBJECT type="text/sitemap">')
            self.__writeLine( '\t\t<param name="Name" value="%s">' % (key))
            self.__writeLine( '\t\t</OBJECT>')

            self.__writeLine('\t<ul>')
            for info in dico[key]:
                typ = info.type
                fic = info.file
                self.__writeLine( '\t\t\t<li> <OBJECT type="text/sitemap">')
                self.__writeLine( '\t\t\t\t<param name="Name" value="%s">' % (key))
                self.__writeLine( '\t\t\t\t<param name="Type" value="%s">' % (typ))
                self.__writeLine( '\t\t\t\t<param name="Local" value="%s">' % (fic))
                self.__writeLine( '\t\t\t\t</OBJECT>')
            self.__writeLine('\t</ul>')

    def __writeMenu(self, infos):
        dico = {}
        for info in infos:
            if info.prnt is None and info.type == Types.module:
                mdl_from_file = os.path.splitext(info.file)[0]
                if info.name != mdl_from_file: continue
            k = info.name
            if k not in dico: dico[k] = []
            dico[k].append( info )
        self.__writeIndex(dico)

    def write(self, cmds):
        self.__writeHdr()
        self.__writeMenu(cmds)
        self.__writeFtr()

def main():
    infos = Infos()
    for f in glob.glob('*.html'):
        infos.readInfos(f)
    infos.tidyInfos()
    w = Writer("h2d2_chm_index.hhk")
    w.write(infos)

main()
